#version 330

in vec4 in_Pos;
in vec2 in_UV;
out vec2 ex_UV;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(void) {
    // pass the position through modified by the transforms
    gl_Position = (projection * view * model) * in_Pos;
    // pass the texture coordinate through
    ex_UV = in_UV;
}
