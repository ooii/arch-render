#version 330

layout(location=0) vec3 in_pos;
layout(location=1) vec3 in_color;

uniform mat4 projection;
uniform mat4 view;

out vec3 vColor;

void main(void) {
	gl_Position = projection*view*vec4(in_pos,1);
	vColor = in_color;
}
