#version 330

in vec3 vColor;

out vec3 color;

void main(void) {
	color = vColor;
}
