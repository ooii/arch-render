#version 330

in vec2 ex_UV;
uniform sampler2D in_Tex;

out vec4 out_Color;

void main(void) {
    out_Color =  texture(in_Tex, ex_UV);
}
