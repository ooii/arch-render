#version 330 core
layout(points) in;
layout(triangle_strip, max_vertices=12) out;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform sampler2DRect gbuff_Color;
uniform sampler2DRect gbuff_Position;
uniform sampler2DRect gbuff_Specular;
in vec3 pos[];
in vec3 color[];
out vec4 gcolor;
void main()
{
	mat4 MVP = projection*view*model;
	vec4 campos = MVP*vec4(pos[0],1);
	vec3 gdir = normalize(texture(gbuff_Specular,campos.xy+0.5).xyz);
	float d = dot(gdir,pos[0]);
	vec3 P2 = pos[0]+gdir*vec3(.2,.4,.6);
	P2 = P2-(dot(gdir,P2)-d)*gdir;
	vec3 v2 = normalize(pos[0]-P2);
	vec3 v3 = normalize(cross(gdir,v2));
	
	vec4 p1 = MVP*vec4(pos[0]+v2+v3,1);
	vec4 p2 = MVP*vec4(pos[0]-v2+v3,1);
	vec4 p3 = MVP*vec4(pos[0]+v2-v3,1);
	vec4 p4 = MVP*vec4(pos[0]-v2-v3,1);
	
	gl_Position = campos;
	gcolor = vec4(color[0],length(color[0]));
	EmitVertex();
	gl_Position = p1;
	gcolor = vec4(color[0],0);
	EmitVertex();
	gl_Position = p2;
	gcolor = vec4(color[0],0);
	EmitVertex();
	EndPrimitive();
	
	gl_Position = campos;
	gcolor = vec4(color[0],length(color[0]));
	EmitVertex();
	gl_Position = p2;
	gcolor = vec4(color[0],0);
	EmitVertex();
	gl_Position = p3;
	gcolor = vec4(color[0],0);
	EmitVertex();
	EndPrimitive();
	
	
	gl_Position = campos;
	gcolor = vec4(color[0],length(color[0]));
	EmitVertex();
	gl_Position = p3;
	gcolor = vec4(color[0],0);
	EmitVertex();
	gl_Position = p4;
	gcolor = vec4(color[0],0);
	EmitVertex();
	EndPrimitive();
	
	
	gl_Position = campos;
	gcolor = vec4(color[0],length(color[0]));
	EmitVertex();
	gl_Position = p4;
	gcolor = vec4(color[0],0);
	EmitVertex();
	gl_Position = p1;
	gcolor = vec4(color[0],0);
	EmitVertex();
	EndPrimitive();
}
