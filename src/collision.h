#ifndef _OBS_COLLISION_H_
#define _OBS_COLLISION_H_

#include <stdlib.h>
#include <string.h>

#include "glibs.h"
#include "vectype.h"
#include "jankscene.h"
#include "parser.h"

class obs_scene;

/*! Triangle or face struct. */
typedef struct obs_tri {
    obs_vec3 a, //!< Triangle vertex position.
             b, //!< Triangle vertex position.
             c; //!< Triangle vertex position.
} obs_col_face;

/*! Struct to hold mesh collider information. */
typedef struct obs_mesh_collider {
    unsigned int num_faces; //!< The number of faces in the mesh collider.
    obs_vec3 center; //!< The centroid for the mesh. Compute with call to computeCentroid().
    obs_col_face *faces; //!< The faces of the mesh (triangles).
} obs_mcol;

/*! Struct to hold a sphere collider. */
typedef struct obs_sphere_collider {
    float radius; //!< Sphere radius
    obs_vec3 center; //!< Sphere center
} obs_sphcol;

/*! Struct to hold a ray hit position. */
typedef struct obs_ray_hit {
    float line_param; //!< Line paramter (t where R(t) = s + vt where R(t) and s are points and v is a normalized direction vector) of the hit.
    obs_vec3 point; //!< The point in space where the hit occurred.
} obs_ray_hit;

/*! Compute the centroid of a piece of geometry. */
obs_vec3 computeCentroid(const obs_vec3 *data, const unsigned int nmemb);
obs_vec3 meshCentroid(const obs_mesh *data);

/*! Cast a ray into the world.  Performs no collisions, simply constructs the ray and returns it. */
obs_ray castRay(const obs_vec3 *origin, const obs_vec3 *dir);

/*! Collide a ray with an arbitrary mesh (face by face assuming triangular meshes). */
unsigned int rayToMesh(const obs_mesh_collider *mesh, const obs_ray *ray);

/*! Collide a ray with a single triangle. */
unsigned int rayToTri(const obs_ray *ray, const obs_col_face *face, obs_ray_hit *hit);

/*! Reflect a ray given a normal. */
float reflectRay(obs_ray *new_ray, const obs_ray *orig, const obs_vec3 *surf_norm);

/*! Bounce a ray in the scene.  The bounces array must be at least of size num_bounces. */
unsigned int bounceRay(const obs_ray *ray, const unsigned int num_bounces, obs_ray_hit *bounces, obs_scene *scene);
#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
