#ifndef __objFile_h__
#define __objFile_h__
#include <tuple>
#include <vector>

struct Vertex {
	// position
	float x,y,z;
	// normal
	float nx,ny,nz;
	// texture
	float u,v;
};

struct BicubicPatchCorner {
	float p0[3], p1[3], p2[3], p3[3];
	float uv[2];
};

class objFile {
private:
	std::vector<std::tuple<float, float, float> > v;
	std::vector<std::tuple<float, float, float> > vn;
	std::vector<std::tuple<float, float> > vt;
	std::vector<std::tuple<unsigned,unsigned,unsigned> > f;
	std::vector<std::tuple<unsigned,unsigned,unsigned> > qf;
	std::vector<std::tuple<unsigned,unsigned,unsigned> > pf;
	void createVboData();
public:
	Vertex *vboData;
	BicubicPatchCorner *patchData;
	unsigned *ibData, *ipData;
	unsigned quadBegin;
	objFile(const char *fname);
	unsigned size()    {return (f.size()+qf.size()+pf.size());}
	unsigned vbosize() {return (f.size()+qf.size())*sizeof(Vertex);}
	unsigned quadSize() { return qf.size()*sizeof(Vertex); }
	unsigned patchsize(){ return (pf.size()/4)*sizeof(BicubicPatchCorner);}
	unsigned ipsize() {return (pf.size()/4)*sizeof(unsigned);}
	unsigned ibsize()  {return (f.size()+qf.size())*sizeof(unsigned);}
	unsigned qCount() {return qf.size();}
	unsigned pCount() {return pf.size()/4;}
	~objFile();
};

#endif
