// general library includes
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>

// local includes
#include "generalmatrix.h"
#include "quaternion.h"
#include "transform.h"
#include "cube.h"

Cube::Cube() {
    this->reset();
    this->num_verts = 8;
}

void Cube::reset() {
    this->verts[0] = Vec4f(1.0f, 1.0f, 1.0f);
    this->verts[1] = Vec4f(1.0f, 1.0f, -1.0f);
    this->verts[2] = Vec4f(1.0f, -1.0f, 1.0f); 
    this->verts[3] = Vec4f(1.0f, -1.0f, -1.0f);
    this->verts[4] = Vec4f(-1.0f, 1.0f, 1.0f);
    this->verts[5] = Vec4f(-1.0f, 1.0f, -1.0f);
    this->verts[6] = Vec4f(-1.0f, -1.0f, 1.0f);
    this->verts[7] = Vec4f(-1.0f, -1.0f, -1.0f);
    const static float temp[] = {1.0f, 0.0f, 0.0f, 0.0f, 
                                 0.0f, 1.0f, 0.0f, 0.0f, 
                                 0.0f, 0.0f, 1.0f, 0.0f, 
                                 0.0f, 0.0f, 0.0f, 1.0f};
    this->_t_matrix = t_matrix(temp);
    this->_q_matrix = t_matrix(temp);
}

std::string Cube::transform() {
    std::string temp = "";
    for (int i = 0; i < this->num_verts; i++) {
        Vec4f temp_q_trans = this->_q_matrix * this->verts[i];
        Vec4f temp_t_trans = this->_t_matrix * this->verts[i];
        std::string temp_q = temp_q_trans.stringify();
        std::string temp_t = temp_t_trans.stringify();
#ifdef DEBUG
        std::cout << "temp_q: " << temp_q << "\ntemp_t: " << temp_t << "\n";
#endif
        //temp += "Normal Transform: \t" + (this->_t_matrix * this->verts[i]).stringify()
        //     + "\nQuaternion Transform: \t" + (this->_q_matrix * this->verts[i]).stringify() + "\n";
        temp += "Normal Transform: \t" + temp_q + "\nQuaternion Transform: \t" + temp_t + "\n";
    }
    temp += "\nMatrix rotations:\n" + this->_t_matrix.stringify();
    temp += "\nQuaternion rotations:\n" + this->_q_matrix.stringify();
    return temp;
}

// translation functions
void Cube::cubeTranslateX(float &amount) {
    t_matrix temp = translateX(amount);
    this->_t_matrix = this->_t_matrix * temp;
    this->_q_matrix = this->_q_matrix * temp;
}

void Cube::cubeTranslateY(float &amount) {
    t_matrix temp = translateY(amount);
    this->_t_matrix = this->_t_matrix * temp;
    this->_q_matrix = this->_q_matrix * temp;
}

void Cube::cubeTranslateZ(float &amount) {
    t_matrix temp;
    this->_t_matrix = this->_t_matrix * temp;
    this->_q_matrix = this->_q_matrix * temp;
}

void Cube::cubeTranslateXYZ(float &dx, float &dy, float &dz) {
    t_matrix temp = translate(dx, dy, dz);
    this->_t_matrix = this->_t_matrix * temp;
    this->_q_matrix = this->_q_matrix * temp;
}

void Cube::cubeTranslateXYZ(Vec4f &v) {
    t_matrix temp = translate(v);
    this->_t_matrix = this->_t_matrix * temp;
    this->_q_matrix = this->_q_matrix * temp;
}

// rotation functions
//rotate about the y axis for heading
void Cube::cubeRotateH(float &amount) {
    // matrix rotation
    this->_t_matrix = this->_t_matrix * matrix_rotateH(amount);

    // quaternion rotation
    this->_q_matrix = this->_q_matrix * rotateH(amount);
}

// rotate about the x axis for pitch
void Cube::cubeRotateP(float &amount) {
    // matrix rotation
    this->_t_matrix = this->_t_matrix * matrix_rotateP(amount);

    // quaternion rotation
    this->_q_matrix = this->_q_matrix * rotateP(amount);
}

// rotate about the z axis for roll
void Cube::cubeRotateR(float &amount) {
    // matrix rotation
    this->_t_matrix = this->_t_matrix * matrix_rotateR(amount);

    // quaternion rotation
    this->_q_matrix = this->_q_matrix * matrix_rotateR(amount);
}

// rotate all 3
void Cube::cubeRotateHPR(float &dh, float &dp, float &dr) {
    // matrix rotation
    this->_t_matrix = this->_t_matrix * matrix_rotate(dh, dp, dr);

    // quaternion rotation
    this->_q_matrix = this->_q_matrix * rotate(dh, dp, dr);
}

// scale
void Cube::cubeScale(float &amount) {
    t_matrix temp = scale(amount);
    this->_t_matrix = temp * this->_t_matrix;
    this->_q_matrix = temp * this->_q_matrix;
}

// combined transform
void Cube::cubeTransformXYZHPRS(float &dx, float &dy, float &dz, float &dh, float &dp, float &dr, float &s) {
    cubeTranslateXYZ(dx, dy, dz);
    cubeRotateHPR(dh, dp, dr);
    cubeScale(s);
}

void showHelp() {
    std::cout << "translateX        amount\n"
              << "translateY        amount\n"
              << "translateZ        amount\n"
              << "translateXYZ      dx dy dz\n"
              << "rotateH           amount // degrees\n"
              << "rotateP           amount // degrees\n"
              << "rotateR           amount // degrees\n"
              << "rotateHPR         dh dp dr // all in degrees\n"
              << "scale             amount\n"
              << "transformXYZHPRS  dx dy dz dh dp dr s\n"
              << "Vector3           1/2 x y z (argument of 1 or 2 indicates which vector to set, 2 available)\n"
              << "Quaternion        1/2 x y z w (again, 1 or 2 indicates which of 2 quaternions to set\n"
              << "SLERP             beta (uses quaternions 1 and 2 as defined with Quaternion command)\n"
              << "LERP              beta (uses Vectors 1 and 2 as defined with Vector3 command)\n"
              << "reset\n"
              << "quit"
              << std::endl;
}

int main() {
    Cube cube = Cube();
    Vec4f vec1 = Vec4f();
    Vec4f vec2 = Vec4f();
    Quaternion quat1 = Quaternion();
    Quaternion quat2 = Quaternion();
    while(1) {
        std::cout << "Enter a function, or type help.\n";
        std::string input;
        std::cin >> input;
        if (input == "translateX") {
            float amt;
            std::cin >> amt;
            cube.cubeTranslateX(amt);
        }
        else if (input == "translateY") {
            float amt;
            std::cin >> amt;
            cube.cubeTranslateY(amt);
        }
        else if (input == "translateZ") {
            float amt;
            std::cin >> amt;
            cube.cubeTranslateZ(amt);
        }
        else if (input == "translateXYZ" || input == "translate") {
            float dx, dy, dz;
            std::cin >> dx >> dy >> dz;
            cube.cubeTranslateXYZ(dx, dy, dz);
        }
        else if (input == "rotateH") {
            float amt;
            std::cin >> amt;
            cube.cubeRotateH(amt);
        }
        else if (input == "rotateP") {
            float amt;
            std::cin >> amt;
            cube.cubeRotateP(amt);
        }
        else if (input == "rotateR") {
            float amt;
            std::cin >> amt;
            cube.cubeRotateR(amt);
        }
        else if (input == "rotateHPR" || input == "rotate") {
            float dh, dp, dr;
            std::cin >> dh >> dp >> dr;
            cube.cubeRotateHPR(dh, dp, dr);
        }
        else if (input == "scale") {
            float amt;
            std::cin >> amt;
            cube.cubeScale(amt);
        }
        else if (input == "transformXYZHPRS" || input == "transform") {
            float dx, dy, dz, dh, dp, dr, s;
            std::cin >> dx >> dy >> dz >> dh >> dp >> dr >> s;
            cube.cubeTransformXYZHPRS(dx, dy, dz, dh, dp, dr, s);
        }
        else if (input == "Vector3") {
            unsigned int flag;
            float x, y, z;
            std::cin >> flag >> x >> y >> z;
            if (flag <= 1) {
                vec1[0] = x;
                vec1[1] = y;
                vec1[2] = z;
            }
            else {
                vec2[0] = x;
                vec2[1] = y;
                vec2[2] = z;
            }
        }
        else if (input == "Quaternion") {
            unsigned int flag;
            float x, y, z, w;
            std::cin >> flag >> x >> y >> z >> w;
            if (flag <= 1) {
                quat1.x() = x;
                quat1.y() = y;
                quat1.z() = z;
                quat1.w() = w;
                quat1.normalize();
            }
            else {
                quat2.x() = x;
                quat2.y() = y;
                quat2.z() = z;
                quat2.w() = w;
                quat2.normalize();
            }
        }
        else if (input == "SLERP") {
            float beta;
            std::cin >> beta;
            Quaternion n = quat1.SLERP(quat2, beta);
            std::cout << "Quaternion 1: " << quat1.q1() << "i + " << quat1.q2() << "j + " << quat1.q3() << "k + " << quat1.q0() << "\n"
                      << "Quaternion 2: " << quat2.q1() << "i + " << quat2.q2() << "j + " << quat2.q3() << "k + " << quat2.q0() << "\n"
                      << "Interpolated: " << n.q1() << "i + " << n.q2() << "j + " << n.q3() << " + " << n.q0() << "\n"
                      << n.matrix().stringify() << std::endl;
        }
        else if (input == "LERP") {
            float beta;
            std::cin >> beta;
            Vec4f n = LERP(vec1, vec2, beta);
            std::cout << "Vector 1: (" << vec1.x() << ", " << vec1.y() << ", " << vec1.z() << ")\n"
                      << "Vector 2: (" << vec2.x() << ", " << vec2.y() << ", " << vec2.z() << ")\n"
                      << "Interpolated: (" << n.x() << ", " << n.y() << ", " << n.z() << ")" << std::endl;
        }
        else if (input == "reset") {
            cube.reset();
        }
        else if (input == "quit" || input == "exit") {
            break;
        }
        else {
            std::cout << "Your input was: " << input << " which isn't recognized.\n"
                      << "Valid functions are: \n\n";
            showHelp();
            continue;
        }
        std::cout << cube.transform() << std::endl;
    }
    return 0;
}
