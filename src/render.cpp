#include "render.h"

// globals/constants
const unsigned int WindowWidth = 800, WindowHeight = 450;
//bool _rendering = true;

void bindTexture(ShaderInfo &shdr) {
    glUniform1i(shdr.sampler, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, shdr.sampler);
}

BufferInfo setupVBO(ObjFile &obj, ShaderInfo &shdr) {
    BufferInfo bffr;
    bffr.ibo_size = obj.faces_size;
    
    // generate the vertex array
    glGenVertexArrays(1, &(bffr.vao));
    checkGLError("Could not generate VAO.", __FILE__, __LINE__);

    // bind the vertex array
    glBindVertexArray(bffr.vao);
    checkGLError("Could not bind VAO.", __FILE__, __LINE__);

    // enable the attribute location for the position (in_Pos) and uv (in_UV)
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    checkGLError("Failed to bind Attribute Locations.", __FILE__, __LINE__);

    // generate the vbo
    glGenBuffers(1, &(bffr.vbo));
    checkGLError("Failed to generate VBO.", __FILE__, __LINE__);

    // bind the vbo
    glBindBuffer(GL_ARRAY_BUFFER, bffr.vbo);
    checkGLError("Failed to bind VBO.", __FILE__, __LINE__);

    // give it the data
    glBufferData(GL_ARRAY_BUFFER, sizeof(obj.verts[0]) * obj.verts_size, (GLvoid*)(obj.verts), GL_STATIC_DRAW);
    checkGLError("Failed to set buffer data for VBO.", __FILE__, __LINE__);

    // specify the structure of the data
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(obj.verts[0]), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(obj.verts[0]), (GLvoid*)(sizeof(obj.verts[0].pos)));
    checkGLError("Failed to set VAO attributes.", __FILE__, __LINE__);

    // generate IBO, buffer object to hold the indices for the faces
    glGenBuffers(1, &(bffr.ibo));
    checkGLError("Failed to generate IBO.", __FILE__, __LINE__);
    
    // bind the IBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bffr.ibo);
    checkGLError("Failed to bind IBO.", __FILE__, __LINE__);

    // send it the index (face) data
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(obj.faces[0]) * obj.faces_size, (GLvoid*)(obj.faces), GL_STATIC_DRAW);
    checkGLError("Failed to set IBO buffer data.", __FILE__, __LINE__);

    // unbind arrays
    glBindVertexArray(0);

    return bffr;
}

void render(ShaderInfo &shdr, BufferInfo &bffr) {
    // static, so only initialized once
    static double t0 = 0; // previously measured time
    static float rotation = 0; // rotation of the model
    // model, view and projection matrices
    static glm::mat4 modelMatrix = glm::mat4(1.0f);
    static glm::mat4 viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    static glm::mat4 projectionMatrix = glm::perspective(60.0f, 16.0f / 9.0f, 0.1f, 100.0f);
 
    float angle = 0.0f; // angle of the model
    double t = glfwGetTime(); // the current time

    // check if t0 is set to a real time, if not, do so
    if (t0 == 0) {
        t0 = t;
    }

    rotation += (float)(t-t0); // rotate based off change in time
    angle = (rotation * 3.1415926f) / 180.0f; // angle in radians
   
    // fill with 1's 
    modelMatrix = glm::mat4(1.0f);
    // rotate about y
    modelMatrix = glm::rotate(modelMatrix, angle, glm::vec3(0, 1, 0));
    // rotate about x
    modelMatrix = glm::rotate(modelMatrix, angle, glm::vec3(1, 0, 0));

    // fill the color and depth buffers with glClearColor
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the shader program given
    glUseProgram(shdr.shaderID);
    checkGLError("Error using shader program.", __FILE__, __LINE__);

    // set the uniforms for the model, view, perspective matrices
    glUniformMatrix4fv(shdr.modelUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));
    glUniformMatrix4fv(shdr.viewUniform, 1, GL_FALSE, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(shdr.projectionUniform, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
    checkGLError("Failed setting uniforms.", __FILE__, __LINE__);

    // bind the IBO to GL_ELEMENT_ARRAY_BUFFER
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bffr.ibo);
#ifdef DEBUG
    printf("Buffer: %d\n", bffr.ibo);
#endif
    checkGLError("Could not bind IBO.", __FILE__, __LINE__);
    
    // bind the vertex array
    glBindVertexArray(bffr.vao);
    checkGLError("Could not bind VAO.", __FILE__, __LINE__);

    // draw the elements based off of the index data
    glDrawElements(GL_TRIANGLES, bffr.ibo_size, GL_UNSIGNED_INT, 0);
    checkGLError("Failed to draw elements.", __FILE__, __LINE__);

    // unbind vertex array and shader
    glBindVertexArray(0);
    glUseProgram(0);
}

void init() {
    // init glfw
    if (!glfwInit()) {
        std::cerr << "Failed to initialize GLFW..." << std::endl;
        exit(EXIT_FAILURE);
    }

    // open window with rendering context
    if (!glfwOpenWindow(WindowWidth, WindowHeight, 8, 8, 8, 8, 16, 0, GLFW_WINDOW)) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    // set a callback function for key presses
    glfwSetKeyCallback(keyEvent);

    // initialize glew
    GLenum initResult = glewInit();
    if (initResult != GLEW_OK) {
        std::cout << "GLEW failed to initialize: " << glewGetErrorString(initResult) << std::endl; exit(EXIT_FAILURE);
    }

    // set the clear color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    checkGLError("Failed to set clear color.", __FILE__, __LINE__);

    // enable depth test
    glEnable(GL_DEPTH_TEST);
    checkGLError("Failed to enable depth test.", __FILE__, __LINE__);
    glDepthFunc(GL_LESS);
    checkGLError("Failed to set depth function.", __FILE__, __LINE__);

    // enable culling on backfaces
    glEnable(GL_CULL_FACE);
    checkGLError("Failed to enable culling.", __FILE__, __LINE__);
    glCullFace(GL_BACK);
    checkGLError("Failed to set backface culling.", __FILE__, __LINE__);
    // just in case the winding order isn't counter-clockwise
    glFrontFace(GL_CCW);
    checkGLError("Failed to set winding order.", __FILE__, __LINE__);
}

void destroyVBO(BufferInfo &bffr) {
    // delete vbo and ibo
    glDeleteBuffers(1, &(bffr.vbo));
    glDeleteBuffers(1, &(bffr.ibo));
    
    // delete vao
    glDeleteVertexArrays(1, &(bffr.vao));

    // set the ibo size appropriately just in case
    bffr.ibo_size = 0;
    checkGLError("Could not destroy VBO.", __FILE__, __LINE__);
}

void initRays(const obs_light &light, obs_ray *rays, unsigned int ray_count, obs_vec3 direction) {
    for (int i = 0; i < ray_count; ++i) {
        // send things in stupid directions
        direction.x += i;
        normalize(&direction, &direction);
        rays[i] = castRay(&(light.pos), &(direction));
    }
}

void testRays(obs_light &light, unsigned int ray_count, unsigned int n_bounces, obs_ray *rays, obs_ray_hit *bounces, obs_scene *scene) {
    for (int i = 0; i < ray_count; ++i) {
        bounceRay(&(rays[i]), n_bounces, bounces, scene);
    }
}

#define NUM_RAYS 5
#define NUM_BOUNCES 3

int main(int argc, char** argv) {
    // read and parse the OBJ file, assumes that nothing horrendous goes wrong
    ObjFile objfile = parseFile("../assets/container.obj");

    // ray inits (lol ray testing)
    obs_game_obj obj;
    obj.mesh = &objfile;

    obs_scene scene;
    scene.objects.push_back(obj);

    obs_vec3 obj_center = meshCentroid(&objfile);
    obs_ray rays[NUM_RAYS];
    obs_ray_hit bounces[NUM_RAYS * NUM_BOUNCES + NUM_RAYS];
    obs_light light;
    light.pos.x = 10;
    light.pos.y = 10;
    light.pos.z = 10;

    initRays(light, rays, NUM_RAYS, obj_center);

    // end ray inits

    // initialize glfw, open a window and init glew/some opengl stuff, returns view matrix
    init();

    // setup the shaders
    ShaderInfo shdr = setupShaders("forward.vert", "forward.frag");
    bindMVP(shdr);
    bindTexture(shdr);

    // setup the buffer objects
    BufferInfo bffr = setupVBO(objfile, shdr);

    // set up the textures
    setupTextures(shdr);

    checkGLProgramError(shdr, __FILE__, __LINE__);

    // render loop
    while (_rendering && glfwGetWindowParam(GLFW_OPENED)) {
        // render the stuff
        render(shdr, bffr);

        // ray testing
        visualize_lights(&light, 1);
        testRays(light, NUM_RAYS, NUM_BOUNCES, rays, bounces, &scene);
        visualize_rays(rays, NUM_RAYS, bounces, NUM_BOUNCES);
        // end ray testing

        // swap front and back buffers
        glfwSwapBuffers();
    }
    
    // destroy everything
    destroyShader(shdr);
    destroyVBO(bffr);

    // terminate glfw if no longer rendering
    glfwTerminate();

    // exit
    return EXIT_SUCCESS;
}
