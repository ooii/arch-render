#ifndef _OBS_TEXTURE_H_
#define _OBS_TEXTURE_H_
#include "glibs.h"

class Texture2D {
public:
	Texture2D(const char *fname);
	~Texture2D();
	inline GLuint operator*() {return uTexture; }
	inline void generateMipmap() {glGenerateMipmap(uTexture); bHasMipmap = true;}
	inline bool hasMipmap() {return bHasMipmap; }
private:
	GLuint uTexture;
	bool bHasMipmap;
};

#endif
