#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>

#define _PI_ 3.14159265
#define RAD(X) X*_PI_/180

// Vector3 class for handling 3D vectors (and also points sometimes)
template <class T> class Vector3 {
    public:
        // default constructor, inits to 0
        Vector3<T>() {
            this->_v_data[0] = 0;
            this->_v_data[1] = 0;
            this->_v_data[2] = 0;
        }
        // constructor
        Vector3<T>(T x, T y, T z) {
            this->_v_data[0] = x;
            this->_v_data[1] = y;
            this->_v_data[2] = z;
        }
        Vector3<T>(T vals[]) {
            this->_v_data[0] = vals[0];
            this->_v_data[1] = vals[1];
            this->_v_data[2] = vals[2];
        }
        Vector3<T>(const Vector3& v) {
            this->_v_data[0] = v._v_data[0];
            this->_v_data[1] = v._v_data[1];
            this->_v_data[2] = v._v_data[2];
        }
        // overload subscript operator to allow access to elements easily
        T& operator[](const int& index) {
            return this->_v_data[index];
        }
        // overload subscript operator for const access
        const T& operator[](const int& index) const {
            return this->_v_data[index];
        }
        // overload parenthesis, also for access
        T& operator()(const int& index) {
            return this->_v_data[index];
        }
        // allow for const access with parentheses
        const T& operator()(const int& index) const {
            return this->_v_data[index];
        }
        const Vector3<T> operator*(const T &val) const {
            return Vector3(this->_v_data[0] * val,
                           this->_v_data[1] * val,
                           this->_v_data[2] * val);
        }
        const Vector3<T> operator+(const Vector3<T> &other) const {
            return Vector3(this->x() + other.x(),
                           this->y() + other.y(),
                           this->z() + other.z());
        }
        inline T& x() {
            return this->_v_data[0];
        }
        inline const T& x() const {
            return this->_v_data[0];
        }
        inline T& y() {
            return this->_v_data[1];
        }
        inline const T& y() const {
            return this->_v_data[1];
        }
        inline T& z() {
            return this->_v_data[2];
        }
        inline const T& z() const {
            return this->_v_data[2];
        }
        // dot product
        T dot(const Vector3<T> &other) {
            return this->_v_data[0]*other[0] +
                   this->_v_data[1]*other[1] +
                   this->_v_data[2]*other[2];
        }
        // cross product
        Vector3<T>& cross(const Vector3<T> &other) {
            return Vector3<T>(this->_v_data[2] * other[1] - this->_v_data[1] * other[2], 
                              this->_v_data[2] * other[0] - this->_v_data[0] - other[2], 
                              this->_v_data[0] * other[1] - this->_v_data[1] * other[0]);
        }
        Vector3<T> LERP(const Vector3<T> &B, const float &beta) const {
            float cbeta = 1 - beta;
            return ((*this) * cbeta) + (B * beta);
        }
        // convert to a string with some formatting (x, y, z)
        std::string stringify() {
            std::stringstream converter;
            converter << "(" << this->_v_data[0] << ", " << this->_v_data[1] << ", " << this->_v_data[2] << ")";
            #ifdef DEBUG
                std::cout << "Vector3 String: " << converter.str();
            #endif
            return converter.str();
        }
    private:
        T _v_data[3];
};

template <class T> const T& LERP(const T& A, const T& B, const float &beta) {
    return (A * (1 - beta)) + (B * beta);
}

// matrix class, handles 4x4 matrices currently
template <class T> class Matrix {
    public:
        // default constructor
        Matrix() {
            // initialize as the identity
            T one = (T)(1);
            T zero = (T)(0);
            static T identity[16] = {one, zero, zero, zero, 
                                     zero, one, zero, zero, 
                                     zero, zero, one, zero, 
                                     zero, zero, zero, one};
            for (int i = 0; i < 16; i++) {
                this->_m_data[i] = identity[i];
            }
        }
        // initialize to be filled with value
        Matrix(const T &val) {
            // initialize to the given value
            for (int i = 0; i < 16; i++) {
                this->_m_data[i] = val;
            }
        }
        // initialize to be populated with values from an array
        Matrix(const T val[]) {
            // initialize to the given value
            for (int i = 0; i < 16; i++) {
                this->_m_data[i] = val[i];
            }
        }
        // overload parenthesis for multi-dimension indexing
        T& operator()(const int row, const int col) {
            return this->_m_data[row * 4 + col];
        }
        const T& operator()(const int row, const int col) const {
            return this->_m_data[row * 4 + col];
        }
        // overload brackets for single dimension access
        T& operator[](const int &index) {
            return this->_m_data[index];
        }
        const T& operator[](const int& index) const {
            return this->_m_data[index];
        }
        // multiplication operator, Vector3 implemented separately because
        //  of dot multiplication vs. cross, whereas matrix has a distinct
        //  mult. operation
        Matrix<T> operator*(const Matrix<T> &other) {
            Matrix<T> temp(0.0f);
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    for (int k = 0; k < 4; k++) { // should replace this with something more efficient
                        temp(i,j) += this->_m_data[i * 4 + k] * other(k, j);
                    }
                }
            }
            return temp;
        }
        // overload mult. operator so we can multiply a matrix times a vector
        Vector3<T> operator*(const Vector3<T> &other) {
            #ifdef DEBUG
                std::cout << "vector mult guys!" << other[0] << other[1] << other[2] << std::endl;
            #endif
            Vector3<T> temp(0, 0, 0);
            for (int i = 0; i < 4; i++) {
                for (int k = 0; k < 3; k++) {
                    temp[i] += this->_m_data[i*4 + k] * other[k];
                }
                temp(3) += this->_m_data[i * 4 + 3];
            }
            return temp;
        }
        // overload *= so we can multiply by scalars
        Matrix<T> operator*=(const T& other) {
            for (int i = 0; i < 16; i++) {
                this->_m_data[i] *= other;
            }
            return *this;
        }
        // convert to a string with some formatting
        std::string stringify() {
            std::stringstream temp;
            //temp << "--\t\t\t--\n"; //i'm naughty, i used tab characters
            temp << std::setfill('-') << std::setw(55) << "\n";
            for (int i = 0; i < 16; i++) {
                temp << "|" << std::setfill(' ') << std::setw(13) << this->_m_data[i++];
                temp << std::setw(13) << this->_m_data[i++];
                temp << std::setw(13) << this->_m_data[i++];
                temp << std::setw(13) << this->_m_data[i] << "|\n";
            }
            temp << std::setfill('-') << std::setw(55) << "\n";
            //temp << "--\t\t\t--";
            #ifdef DEBUG
                std::cout << "stringify" << std::endl;
                std::cout << "temp.str" << temp.str() << std::endl;
            #endif
            return temp.str();
        }
    private:
        // represented as 1D array, index in by row*width + col
        T _m_data[16];
};

// declaration of the cube class, which handles the transforms
class Cube {
    public:
        Cube();
        // translate operations
        void translateX(float amount);
        void translateY(float amount);
        void translateZ(float amount);
        void translateXYZ(float dx, float dy, float dz);
        void translateXYZ(Vector3<float> v);
        // rotation operations
        void rotateH(float amount);
        void rotateP(float amount);
        void rotateR(float amount);
        void rotateHPR(float dh, float dr, float dp);
        // scale operations
        void scale(float amount);
        // general transform
        void transformXYZHPRS(float dx, float dy, float dz, 
                              float dh, float dr, float dp, 
                              float s);
        // reset to defaults
        void reset();
        // apply the transform and return a string representation
        std::string transform();
    private:
        Matrix<float> _t_matrix; // the transformation matrix
        Matrix<float> _q_matrix; // the alternate matrix using quaternions for rotations
        Vector3<float> verts[8]; // the vertices
        size_t num_verts; // the number of vertices
};

#endif
