#ifndef _OBS_MATRIX_H_
#define _OBS_MATRIX_H_

#include <string>
#include <iomanip>
#include <sstream>
#include <math.h>

template <class T, unsigned int _R, unsigned int _C> class Matrix {
    public:
        // public constructors
        Matrix() {
            for (int i = 0; i < _R * _C; i++) {
                if (i % _R == i / _R) { // pretend it's square in the number of rows
                    this->_data[i] = 1;
                }
                else {
                    this->_data[i] = 0;
                }
            }
        }
        Matrix(const T &dx, const T &dy, const T &dz) {
            this->_data[0] = dx;
            this->_data[1] = dy;
            this->_data[2] = dz;
            if (_R * _C >= 4) {this->_data[3] = 1;}
        }
        Matrix(const Matrix<T,_R,_C> &mat) {
            for (int i = 0; i < _R * _C; i++) {
                this->_data[i] = mat._data[i];
            }
        }
        Matrix(const T &val) {
            for (int i = 0; i < _R * _C; i++) {
                this->_data[i] = val;
            }
        }
        Matrix(const T val[_R * _C]) {
            for (int i = 0; i < _R * _C; i++) {
                this->_data[i] = val[i];
            }
        }
        
        // access operators
        inline T& operator()(const unsigned int &row, const unsigned int &col) {
            return this->_data[row * _R + col];
        }
        inline const T& operator()(const unsigned int &row, const unsigned int &col) const {
            return this->_data[row * _R + col];
        }
        inline T& operator[](const int &index) {
            return this->_data[index];
        }
        inline const T& operator[](const int &index) const {
            return this->_data[index];
        }
        inline T& x() {
            return this->_data[0];
        }
        inline const T& x() const {
            return this->_data[0];
        }
        inline T& y() {
            return this->_data[1];
        }
        inline const T& y() const {
            return this->_data[1];
        }
        inline T& z() {
            return this->_data[2];
        }
        inline const T& z() const {
            return this->_data[2];
        }
        inline T& w() {
            return this->_data[3];
        }
        inline const T& w() const {
            return this->_data[3];
        }
        inline T& q1() {
            return this->_data[0];
        }
        inline const T& q1() const {
            return this->_data[0];
        }
        inline T& q2() {
            return this->_data[1];
        }
        inline const T& q2() const {
            return this->_data[1];
        }
        inline T& q3() {
            return this->_data[2];
        }
        inline const T& q3() const {
            return this->_data[2];
        }
        inline T& q0() {
            return this->_data[3];
        }
        inline const T& q0() const {
            return this->_data[3];
        }

        // arithmatic operators
        inline Matrix<T, _R, _C> operator+(const Matrix<T, _R, _C> &B) {
            Matrix<T, _R, _C> temp = Matrix<T, _R, _C>((T)(0));
            for (int i = 0; i < _R * _C; i++) {
                temp[i] = this->_data[i] + B[i];
            }
            return temp;
        }
        inline Matrix<T, _R, _C> operator-(const Matrix<T, _R, _C> &B) {
            Matrix<T, _R, _C> temp = Matrix<T, _R, _C>((T)(0));
            for (int i = 0; i < _R * _C; i++) {
                temp[i] = this->_data[i] - B[i];
            }
            return temp;
        }
        template <unsigned int _C2> inline Matrix<T, _R, _C2> operator*(const Matrix<T, _C, _C2> &other) {
            Matrix<T, _R, _C2> temp((T)(0));
            for (int i = 0; i < _R; i++) {
                for (int j = 0; j < _C2; j++) {
                    for (int k = 0; k < _C; k++) {
                        temp(i, j) += this->_data[i * _R + k] * other(k, j);
                    }
                }
            }
            return temp;
        }
        inline Matrix<T, _R, _C>& operator*=(const T &other) {
            for (int i = 0; i < _R * _C; i++) {
                this->_data[i] *= other;
            }
            return *this;
        }
        friend inline Matrix<T, _R, _C>& operator*(const Matrix<T, _R, _C> &mat, const T &val);
        friend inline Matrix<T, _R, _C>& operator*(const T &val, const Matrix<T, _R, _C> &mat);
        Matrix<T, _R, _C> operator*(const T& val) {
            for (int i = 0; i < _R * _C; i++) {
                this->_data[i] *= val;
            }
            return (*this);
        }
        // VecN operations
        friend inline T& dot(const Matrix<T, _R, _C> &A, const Matrix<T, _R, _C> &B);
        T& magnitude() {
            T squared_m = (T)(0);
            for (int i = 0; i < _R * _C; i++) {
                squared_m += this->_data[i] * this->_data[i];
            }
            return sqrt(squared_m);
        }
        Matrix<T, _R, _C>& normalize() {
            return (*this) *= this->magnitude();
        }
        
        // Vec3 operations
        friend inline T dot3(const Matrix<T, _R, _C> &A, const Matrix<T, _R, _C> &B);
        T magnitude3() {
            T squared_m = (T)(0);
            for (int i = 0; i < 3; i++) {
                squared_m += this->_data[i] * this->_data[i];
            }
            return sqrt(squared_m);
        }

        // convert to a string
        std::string stringify() {
            std::stringstream temp;
            temp << std::setfill('-') << std::setw(55) << "\n";
            for (int i = 0; i < 16; i++) {
                temp << "|" << std::setfill(' ') << std::setw(13) << this->_data[i++];
                temp << std::setw(13) << this->_data[i++];
                temp << std::setw(13) << this->_data[i++];
                temp << std::setw(13) << this->_data[i] << "|\n";
            }
            temp << std::setfill('-') << std::setw(55) << "\n";
            return temp.str();
        }
    private:
        T _data[_R * _C];
};

// friends for handling scalar multiplication
template <class T, unsigned int _R, unsigned int _C> inline Matrix<T, _R, _C> operator*(const Matrix<T, _R, _C> &mat, const T &val) {
    Matrix<T, _R, _C> temp;
    for (int i = 0; i < _R * _C; i++) {
        temp[i] = mat[i] * val;
    }
    return temp;
}
template <class T, unsigned int _R, unsigned int _C> inline Matrix<T, _R, _C> operator*(const T &val, const Matrix<T, _R, _C> &mat) {
    Matrix<T, _R, _C> temp;
    for (int i = 0; i < _R * _C; i++) {
        temp[i] = mat[i] * val;
    }
    return temp;
    //return mat * val;
}

// dot product
//  NOTE: assumes that either _R or _C is 1
template <class T, unsigned int _R, unsigned int _C> inline T dot(const Matrix<T, _R, _C> &A, const Matrix<T, _R, _C> &B) {
    T &temp = (T)(0);
    for (unsigned int i = 0; i < _R * _C; i++) {
        temp += A[i] * B[i];
    }
    return temp;
}

template <class T, unsigned int _R, unsigned int _C> inline T dot3(const Matrix<T, _R, _C> &A, const Matrix<T, _R, _C> &B) {
    T &temp = (T)(0);
    for (unsigned int i = 0; i < 3; i++) {
        temp += A[i] * B[i];
    }
    return temp;
}

// Linear Interpolate
//  NOTE: Doesn't check if it's valid to LERP between A and B
template<class T, unsigned int _R, unsigned int _C> inline Matrix<T, _R, _C> LERP(const Matrix<T, _R, _C> &A, const Matrix<T, _R, _C> &B, const float &beta) {
    return ((1-beta) * A) + (beta * B);
}

// Spherical Interpolate
//  NOTE: Assumes valid to SLERP between A and B.
template<class T, unsigned int _R, unsigned int _C> inline Matrix<T, _R, _C> SLERP(const Matrix<T, _R, _C> &p, const Matrix<T, _R, _C> &q, const float &B) {
    float theta = acos(p.dot3(q));
    float sinth = sin(theta);
    return (p * (sin((1-B) * theta) / sinth)) + (q * (sin(B * theta) / sinth));
}

typedef Matrix<float, 4, 1> Vec4f;
typedef Matrix<float, 3, 1> Vec3f;
typedef Matrix<float, 4, 4> t_matrix;
#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
