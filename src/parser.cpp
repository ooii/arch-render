#include "parser.h"

#ifdef TESTING
void printTest(ObjFile result) {
    for (int i = 0; i < result.verts_size; i++) {
        std::cout << "v "
            << result.verts[i].pos[0] << " " 
            << result.verts[i].pos[1] << " " 
            << result.verts[i].pos[2] << "\n";
    }
    for (int i = 0; i < result.verts_size; i++) {
        std::cout << "vt "
            << result.verts[i].uv[0] << " "
            << result.verts[i].uv[1] << "\n";
    }
    for (int i = 0; i < result.faces_size; i++) {
        std::cout << "f "
            << result.faces[i++] << " "
            << result.faces[i++] << " "
            << result.faces[i] << "\n";
    }
}
#endif

#ifdef DEBUG
void printResult(ObjFile result) {
    for (int i = 0; i < result.verts_size; i++) {
        std::cout << "Vert " << i << ": (" 
            << result.verts[i].pos[0] << ", " 
            << result.verts[i].pos[1] << ", " 
            << result.verts[i].pos[2] << ")\n\t" << "UV: (" 
            << result.verts[i].uv[0] << ", " 
            << result.verts[i].uv[1] << ")\n";
    }
    for (int i = 0; i < result.faces_size; i++) {
        std::cout << "Face " << i/3 << ": ("
            << result.faces[i++] << ", "
            << result.faces[i++] << ", "
            << result.faces[i] << ")\n";
    }
}
#endif

// fgets

//ObjFile parseFile(std::string fname) {
//    
//}
//

ObjFile parseFile(std::string fname) {
    std::ifstream counter(fname.c_str()), parser;
    ObjFile result;
    unsigned int numUVs = 0, vertInd = 0, faceInd = 0, uvInd = 0;
    struct UV {
        float coord[2];
    };
    UV *tempUVs;
    result.verts_size = 0;
    result.faces_size = 0;
    std::string input;

    // count the number of vertices and faces
    while (counter >> input) {
        if (input == "v") {
            ++result.verts_size;
        }
        else if (input == "vt") {
            ++numUVs;
        }
        else if (input == "f") {
            ++result.faces_size;
        }
    }
    result.faces_size *= 3;
    // allocate array of vertices and array of faces on the heap
    result.verts = new Vertex[result.verts_size];
    result.faces = new unsigned int[result.faces_size]; // need 3 indices per face
    tempUVs = new UV[numUVs];
    
    // read in the vertex data and face data
    parser.open(fname.c_str());
    while (parser >> input) { // splits on whitespace
        if (input == "v") { // encountered a vertex
            float val; // value to store the parsed float
            for (int i = 0; i < 3 && parser >> val; i++) {
                // assign the vertex value
                result.verts[vertInd].pos[i] = val;
            }
            result.verts[vertInd].pos[3] = 1.0f;

            // assign the uvs to filler values for now, guarantee initialization
            result.verts[vertInd].uv[0] = 0.0f;
            result.verts[vertInd].uv[1] = 0.0f;
            ++vertInd;
        }
        else if (input == "vt") {
            float val;
            for (int i = 0; i < 2 && parser >> val; i++) {
                tempUVs[uvInd].coord[i] = val;
            }
            ++uvInd;
        }
    //}

    // return to beginning, only read face data once we have all the vertex and texture data
    //parser.seekg(0, ios::beg);
    //while (parser >> input) {
        else if (input == "f") {
            std::string val;
            unsigned int vert, uv, normal; // value to store the parsed int
            for (int i = faceInd; faceInd - i < 3 && parser >> val; faceInd++) {
                // scanf the values out of the string
                //sscanf(val.c_str(), "%u/%u/%u", &vert, &uv, &normal);
                if (sscanf(val.c_str(), "%u/%u/%u", &vert, &uv, &normal) == 3) {
                    #ifdef DEBUG
                        printf("Read %u/%u/%u in vert/uv/normal format.\n", vert, uv, normal);
                    #endif
                }
                else if (sscanf(val.c_str(), "%u/%u", &vert, &uv) == 3) {
                    #ifdef DEBUG
                        printf("Read %u/%u in vert/uv format.\n", vert, uv);
                    #endif
                }
                else if (sscanf(val.c_str(), "%u//%u", &vert, &normal) == 3) {
                    #ifdef DEBUG
                        printf("Read %u//%u in vert//normal format.\n", vert, normal);
                    #endif
                }
                else if (sscanf(val.c_str(), "%u", &vert) == 3) {
                    #ifdef DEBUG
                        printf("Read %u in vert format.\n", vert);
                    #endif
                }

                // obj indexes from 1
                --vert;
                --uv;
                --normal;

                // set the face value to be the vert index
                result.faces[faceInd] = vert;

                // we finally have the data for the uv, use it
                result.verts[vert].uv[0] = tempUVs[uv].coord[0];
                result.verts[vert].uv[1] = tempUVs[uv].coord[1];
            }
            //++faceInd;
        }
    }
    delete [] tempUVs; // delete tempUVs
#ifdef DEBUG
    printResult(result);
#endif
#ifdef TESTING
    printTest(result);
#endif
    return result;
}
