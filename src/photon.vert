#version 330

layout(location=0) in vec3 ipos;
layout(location=1) in vec3 icolor;

out vec3 pos;
out vec3 color;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
void main(void) {
	//gl_Position = projection*view*model*vec4(ipos,1);
    gl_Position = ipos;
	pos = ipos;
	color = icolor;
}
