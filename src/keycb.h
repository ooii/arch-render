#ifndef _OBS_KEYCB_H_
#define _OBS_KEYCB_H_

#include "glibs.h"

bool _rendering = true;

void GLFWCALL keyEvent(int key, int action) {
    if (key == GLFW_KEY_ESC && action == GLFW_PRESS) {
        _rendering = false;
    }
}

#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
