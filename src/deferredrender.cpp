#include "deferredrender.h"
#include "gshader.h"

BufferInfo setupVBO(objFile &obj, ShaderInfo &shdr) {
    checkGLError("Enter Setup VBO.",__FILE__,__LINE__);
    BufferInfo bffr;
    bffr.ibo_size = obj.ibsize()/sizeof(unsigned);
    // generate the vertex array
    glGenVertexArrays(1, &(bffr.vao));
    checkGLError("Could not generate VAO.", __FILE__, __LINE__);

    // bind the vertex array
    glBindVertexArray(bffr.vao);
    checkGLError("Could not bind VAO.", __FILE__, __LINE__);

    // enable the attribute location for the position (in_Pos) and uv (in_UV)
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    checkGLError("Failed to bind Attribute Locations.", __FILE__, __LINE__);

    // generate the vbo
    glGenBuffers(1, &(bffr.vbo));
    checkGLError("Failed to generate VBO.", __FILE__, __LINE__);

    // bind the vbo
    glBindBuffer(GL_ARRAY_BUFFER, bffr.vbo);
    checkGLError("Failed to bind VBO.", __FILE__, __LINE__);

    // give it the data
    //glBufferData(GL_ARRAY_BUFFER, sizeof(obj.verts[0]) * obj.verts_size, (GLvoid*)(obj.verts), GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, obj.vbosize(), (GLvoid*)(obj.vboData), GL_STATIC_DRAW);
    checkGLError("Failed to set buffer data for VBO.", __FILE__, __LINE__);

    // specify the structure of the data
    //glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(obj.verts[0]), 0);
    //glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(obj.verts[0]), (GLvoid*)(sizeof(obj.verts[0].pos)));
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(sizeof(float)*6));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(sizeof(float)*3));
    checkGLError("Failed to set VAO attributes.", __FILE__, __LINE__);

    // generate IBO, buffer object to hold the indices for the faces
    glGenBuffers(1, &(bffr.ibo));
    checkGLError("Failed to generate IBO.", __FILE__, __LINE__);
    
    // bind the IBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bffr.ibo);
    checkGLError("Failed to bind IBO.", __FILE__, __LINE__);

    // send it the index (face) data
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(obj.faces[0]) * obj.faces_size, (GLvoid*)(obj.faces), GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, obj.ibsize(), (GLvoid*)(obj.ibData), GL_STATIC_DRAW);
    checkGLError("Failed to set IBO buffer data.", __FILE__, __LINE__);

    // unbind arrays
    glBindVertexArray(0);

    return bffr;
}

void initScreenQuad(Screen &scrn) {
    // quad for the screen in 2 CCW wound triangles
    static const GLfloat screen_quad_vert[] = {
        -1.0f,  -1.0f,  0.0f,
        1.0f,   -1.0f,  0.0f,
        -1.0f,  1.0f,   0.0f,
        -1.0f,  1.0f,   0.0f,
        1.0f,   -1.0f,  0.0f,
        1.0f,   1.0f,   1.0f
    };
    
    // generate the vertex array
    glGenVertexArrays(1, &(scrn.vao));
    checkGLError("Could not generate VAO.", __FILE__, __LINE__);

    // bind the vertex array
    glBindVertexArray(scrn.vao);
    checkGLError("Could not bind VAO.", __FILE__, __LINE__);

    // enable the attribute location for the position (in_Pos) and uv (in_UV)
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    checkGLError("Failed to bind Attribute Locations.", __FILE__, __LINE__);

    // generate the vbo
    glGenBuffers(1, &(scrn.vbo));
    checkGLError("Failed to generate VBO.", __FILE__, __LINE__);

    // bind the vbo
    glBindBuffer(GL_ARRAY_BUFFER, scrn.vbo);
    checkGLError("Failed to bind VBO.", __FILE__, __LINE__);

    // give it the data
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 18, (GLvoid*)(screen_quad_vert), GL_STATIC_DRAW);
    checkGLError("Failed to set buffer data for VBO.", __FILE__, __LINE__);

    // specify the structure of the data
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 3*sizeof(float), 0);
    checkGLError("Failed to set VAO attributes.", __FILE__, __LINE__);
}

// destroy the buffer object
void destroyVBO(BufferInfo &bffr) {
    // delete vbo and ibo
    glDeleteBuffers(1, &(bffr.vbo));
    glDeleteBuffers(1, &(bffr.ibo));
    
    // delete vao
    glDeleteVertexArrays(1, &(bffr.vao));

    // set the ibo size appropriately just in case
    bffr.ibo_size = 0;
    checkGLError("Could not destroy VBO.", __FILE__, __LINE__);
}

void init(Screen &scrn) {
    // init glfw
    if (!glfwInit()) {
        std::cerr << "Failed to initialize GLFW..." << std::endl;
        exit(EXIT_FAILURE);
    }

    // open window with rendering context
    if (!glfwOpenWindow(scrn.width, scrn.height, 8, 8, 8, 8, 16, 0, GLFW_WINDOW)) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    // set a callback function for key presses
    glfwSetKeyCallback(keyEvent);

    // initialize glew
    GLenum initResult = glewInit();
    if (initResult != GLEW_OK) {
        std::cout << "GLEW failed to initialize: " << glewGetErrorString(initResult) << std::endl; exit(EXIT_FAILURE);
    }

    // set the clear color
    glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
    checkGLError("Failed to set clear color.", __FILE__, __LINE__);

    // enable depth test
    glEnable(GL_DEPTH_TEST);
    checkGLError("Failed to enable depth test.", __FILE__, __LINE__);
    glDepthFunc(GL_LESS);
    checkGLError("Failed to set depth function.", __FILE__, __LINE__);

    // enable culling on backfaces
    //glEnable(GL_CULL_FACE);
    checkGLError("Failed to enable culling.", __FILE__, __LINE__);
    ///glCullFace(GL_BACK);
    checkGLError("Failed to set backface culling.", __FILE__, __LINE__);
    // just in case the winding order isn't counter-clockwise
    glFrontFace(GL_CCW);
    checkGLError("Failed to set winding order.", __FILE__, __LINE__);

    initScreenQuad(scrn);
}

void drawScreen(ShaderInfo &shdr, BufferInfo &bffr, Screen &scrn, GBuffer &gbuff) {
#ifdef VERBOSE
    checkProgram(shdr.shaderID, shdr.frag, shdr.vert);
#endif
    checkGLError("Top of drawScreen.", __FILE__, __LINE__);
    // use the program
    glUseProgram(scrn.shader.shaderID);
    checkGLProgramError(scrn.shader, __FILE__, __LINE__);
    // bind the appropriate texture uniforms
    bindGbuffTextures(gbuff);
    // bind to the default framebuffer
	checkGLError("Error before framebuffer.", __FILE__, __LINE__);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkGLError("Error using framebuffer.", __FILE__, __LINE__);
	checkGLFramebufferError(__FILE__,__LINE__);
    // set the viewport to the full screen
    checkGLError("Viewport error.", __FILE__, __LINE__);

    // use the deferred shader program
    glUseProgram(scrn.shader.shaderID);
    checkGLError("Error using shader program.", __FILE__, __LINE__);

    // bind the vbo
    glBindBuffer(GL_ARRAY_BUFFER, scrn.vbo);
    checkGLError("Failed to bind VBO.", __FILE__, __LINE__);

    // bind the vertex array
    glBindVertexArray(scrn.vao);
    checkGLError("Could not bind VAO.", __FILE__, __LINE__);
	
	GLint photonLocations = glGetUniformLocation(scrn.shader.shaderID,"photonLocs");
    checkGLError("Failed to get photon locations.", __FILE__, __LINE__);
	GLint photonColors = glGetUniformLocation(scrn.shader.shaderID,"photonColors");
    checkGLError("Failed to get photon colors.", __FILE__, __LINE__);
	GLint photonCount = glGetUniformLocation(scrn.shader.shaderID,"photonCount");
    checkGLError("Failed to get photon colors.", __FILE__, __LINE__);
	GLfloat locs[] = {scrn.width/2.2,scrn.height/4, scrn.width/2,scrn.height/3, scrn.width/1.8,scrn.height/4.1};
	GLfloat colors[] = {1,1,1, 0,0,1, 1,1,0};
	int count = sizeof(colors)/(sizeof(GLfloat)*3);
	glUniform2fv(photonLocations, count, locs);
    checkGLError("Failed to set photon locations.", __FILE__, __LINE__);
	glUniform3fv(photonColors, count, colors);
    checkGLError("Failed to set photon colors.", __FILE__, __LINE__);
	glUniform1i(photonCount,count);
    checkGLError("Failed to set photon count.", __FILE__, __LINE__);

	GLint ambient = glGetUniformLocation(scrn.shader.shaderID,"ambient");
    checkGLError("Failed to get ambient color location.", __FILE__, __LINE__);
	glUniform3f(ambient,0.2,0.2,0.2);
    checkGLError("Failed to set photon count.", __FILE__, __LINE__);

    // draw the elements based off of the index data
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    checkGLError("Failed to set draw buffers.", __FILE__, __LINE__);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    checkGLError("Failed to draw elements.", __FILE__, __LINE__);

    // unbind vertex array and shader
    glBindVertexArray(0);
    glUseProgram(0);
}
glm::mat4 modelMatrix, viewMatrix, projectionMatrix;
void drawPhotons(program &shdr, BufferInfo &bffr, Screen &scrn, GBuffer &gbuff) {
#ifdef VERBOSE
    checkProgram(shdr.shaderID, shdr.frag, shdr.vert);
#endif
    checkGLError("Top of drawScreen.", __FILE__, __LINE__);
    // use the program
    glUseProgram(*shdr);
	checkGLError("Error binding shader.", __FILE__, __LINE__);
    // bind the appropriate texture uniforms
    bindGbuffTextures(gbuff);
    // bind to the default framebuffer
	checkGLError("Error before framebuffer.", __FILE__, __LINE__);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkGLError("Error using framebuffer.", __FILE__, __LINE__);
	checkGLFramebufferError(__FILE__,__LINE__);
    // set the viewport to the full screen
    checkGLError("Viewport error.", __FILE__, __LINE__);

    // use the deferred shader program
    glUseProgram(*shdr);
    checkGLError("Error using shader program.", __FILE__, __LINE__);

    // bind the vbo
    glBindBuffer(GL_ARRAY_BUFFER, scrn.vbo);
    checkGLError("Failed to bind VBO.", __FILE__, __LINE__);

    // bind the vertex array
    glBindVertexArray(scrn.vao);
    checkGLError("Could not bind VAO.", __FILE__, __LINE__);

    // draw the elements based off of the index data
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    checkGLError("Failed to set draw buffers.", __FILE__, __LINE__);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    checkGLError("Failed to draw elements.", __FILE__, __LINE__);

    // unbind vertex array and shader
    glBindVertexArray(0);
    glUseProgram(0);
}
struct Photon {
	float x,y,z;
	float r,g,b;
};
BufferInfo setupPhotonVBO() {
    BufferInfo bffr;
    bffr.ibo_size = 1;
    // generate the vertex array
    glGenVertexArrays(1, &(bffr.vao));
    checkGLError("Could not generate VAO.", __FILE__, __LINE__);

    // bind the vertex array
    glBindVertexArray(bffr.vao);
    checkGLError("Could not bind VAO.", __FILE__, __LINE__);

    // enable the attribute location for the position (in_Pos) and uv (in_UV)
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    checkGLError("Failed to bind Attribute Locations.", __FILE__, __LINE__);

    // generate the vbo
    glGenBuffers(1, &(bffr.vbo));
    checkGLError("Failed to generate VBO.", __FILE__, __LINE__);

    // bind the vbo
    glBindBuffer(GL_ARRAY_BUFFER, bffr.vbo);
    checkGLError("Failed to bind VBO.", __FILE__, __LINE__);

    // give it the data
	Photon test = {-0.287151, -0.437144, 0.303480, 1,1,1};
	float test2[] = {0,0,0, 1,1,1,1,
				  -0.5f,-0.5f,0.0f, 1,1,1,0,
				   0.5f,-0.5f,0.0f, 1,1,1,0,
				   0,0,0, 1,1,1,1,
				   0.5f,-0.5f,0.0f, 1,1,1,0,
				  -0.5f,0.5f,0.0f, 1,1,1,0,
					0,0,0, 1,1,1,1,
					-0.5f,  0.5f,   0.0f, 1,1,1,0,
					0.5f,   -0.5f,  0.0f, 1,1,1,0,
					0,0,0, 1,1,1,1,
					0.5f,   -0.5f,  0.0f, 1,1,1,0,
					0.5f,   0.5f,   0.0f, 1,1,1,0};

    //glBufferData(GL_ARRAY_BUFFER, sizeof(obj.verts[0]) * obj.verts_size, (GLvoid*)(obj.verts), GL_STATIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(Photon), (GLvoid*)(&test), GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(test2), (GLvoid*)(test2), GL_STATIC_DRAW);
    checkGLError("Failed to set buffer data for VBO.", __FILE__, __LINE__);

    // specify the structure of the data
    //glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(obj.verts[0]), 0);
    //glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(obj.verts[0]), (GLvoid*)(sizeof(obj.verts[0].pos)));
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Photon), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Photon), (GLvoid*)(sizeof(float)*3));
    checkGLError("Failed to set VAO attributes.", __FILE__, __LINE__);

    // unbind arrays
    glBindVertexArray(0);

    return bffr;
}

void render(ShaderInfo &shdr, BufferInfo &bffr, GBuffer &gbuff, Screen &scrn) {
    // static, so only initialized once
    static double t0 = 0; // previously measured time
    static float rotation = 0; // rotation of the model
    // model, view and projection matrices
    modelMatrix = glm::mat4(1.0f);
    viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    projectionMatrix = glm::perspective(60.0f, 16.0f / 9.0f, 0.1f, 100.0f);
 
	static int runs = 0;
    float angle = 0.0f; // angle of the model
    static double t; // the current time

    t = glfwGetTime();

    // check if t0 is set to a real time, if not, do so
    if (t0 == 0) {
        t0 = t;
    }

    //rotation += 450.f * (float)(t-t0); // rotate based off change in time
    angle = (rotation * 3.1415926) / 180.0f; // angle in radians
    t0 = t;

    // bind the right frame buffer
    glBindFramebuffer(GL_FRAMEBUFFER, gbuff.gbuff);
    checkGLError("Error binding framebuffer", __FILE__, __LINE__);
	checkGLFramebufferError(__FILE__, __LINE__);
    // fill the color and depth buffers with glClearColor
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    checkGLError("Error clearing.", __FILE__, __LINE__);
    // render the whole framebuffer
    //glViewport(0, 0, scrn.width, scrn.height);
    //checkGLError("Viewport error.", __FILE__, __LINE__);
   
    // fill with identity matrix
    modelMatrix = glm::mat4(1.0f);
    // rotate about y
    modelMatrix = glm::rotate(modelMatrix, angle, glm::vec3(0, 1, 0));
    // rotate about x
    modelMatrix = glm::rotate(modelMatrix, angle, glm::vec3(1, 0, 0));

    // use the shader program given
    glUseProgram(shdr.shaderID);
    checkGLError("Error using shader program.", __FILE__, __LINE__);

    // set the uniforms for the model, view, perspective matrices
    glUniformMatrix4fv(shdr.modelUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));
    glUniformMatrix4fv(shdr.viewUniform, 1, GL_FALSE, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(shdr.projectionUniform, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

    // bind the proper texture
    bindShaderTexture(shdr);

    // bind the IBO to GL_ELEMENT_ARRAY_BUFFER
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bffr.ibo);
    checkGLError("Could not bind IBO.", __FILE__, __LINE__);
    
    // bind the vertex array
    glBindVertexArray(bffr.vao);
    checkGLError("Could not bind VAO.", __FILE__, __LINE__);
	
	const GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
	glDrawBuffers(3, buffers);
	// samples query
	GLuint query;
	glGenQueries(1,&query);
	glBeginQuery(GL_SAMPLES_PASSED,query);

    // draw the elements based off of the index data
    glDrawElements(GL_TRIANGLES, bffr.ibo_size, GL_UNSIGNED_INT, 0);
    checkGLError("Failed to draw elements.", __FILE__, __LINE__);

	// end sample query
	GLint samples = GL_FALSE;
	//glGetQueryiv(GL_SAMPLES_PASSED,GL_CURRENT_QUERY,&samples);
	glEndQuery(GL_SAMPLES_PASSED);
	if (runs++==0) {
		while (samples==GL_FALSE) {
			glGetQueryObjectiv(query,GL_QUERY_RESULT_AVAILABLE,&samples);
		}
		glGetQueryObjectiv(query,GL_QUERY_RESULT,&samples);
		printf("Samples Passed: %i\n",samples);
	}
	glDeleteQueries(1,&query);

    // unbind vertex array and shader
    glBindVertexArray(0);
    glUseProgram(0);
}

int main(int argc, char **argv) {
    GBuffer gbuff;
    GLenum drawBuffers[] = {GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1,GL_COLOR_ATTACHMENT2};
    ShaderInfo shdr;
    Screen scrn;
    BufferInfo bffr, photons;
#ifdef _MSC_VER
    //ObjFile obj = parseFile("C:/Users/matt/arch-render/assets/container.obj");
    objFile obj("C:/Users/matt/arch-render/assets/container.obj");
#else
    //ObjFile obj = parseFile("../assets/container.obj");
    objFile obj("../assets/container.obj");
#endif

#ifdef DEBUG
    obsErrorMsg("Before init", __FILE__, __LINE__);
#endif
    
    // default screen width/height
    scrn.width = 1280;
    scrn.height = 768;

    // if the arguments are given for width and height, set them accordingly
    // this isn't going to be part of the final api (i think...) but it's useful for now
    if (argc == 3) {
        scrn.width  = atoi(argv[1]);
        scrn.height = atoi(argv[2]);
    }
	glViewport(0, 0, scrn.width, scrn.height);

    // general init function
    init(scrn);
#ifdef _MSC_VER
	photons = setupPhotonVBO();
	program photonShdr("C:/Users/matt/arch-render/src/photon2.vert","C:/Users/matt/arch-render/src/photon.frag");
#else 
	photons = setupPhotonVBO();
	program photonShdr("photon2.vert","photon.frag");
#endif
#ifdef DEBUG
    obsErrorMsg("Initialized", __FILE__, __LINE__);
#endif
    
#ifdef _MSC_VER
    // for matt
    // your problem is that visual studio's working directory isn't the location of the source
    shdr = setupShaders("C:/Users/matt/arch-render/src/basic.vert", "C:/Users/matt/arch-render/src/basic.frag");
    bindMVP(shdr);
    scrn.shader = setupShaders("C:/Users/matt/arch-render/src/deferred.vert", "C:/Users/matt/arch-render/src/deferred.frag");
    checkGLError("Before Setup Textures.",__FILE__,__LINE__);
    setupTextures(shdr, "C:/Users/matt/arch-render/assets/container_diffuse.tga");
#else
    shdr = setupShaders("basic.vert", "basic.frag");
    bindMVP(shdr);
    scrn.shader = setupShaders("deferred.vert", "deferred.frag");
    checkGLError("Before Setup Textures.",__FILE__,__LINE__);
    setupTextures(shdr, "../assets/container_diffuse.tga");
#endif
    //checkGLError("Before Setup Textures.",__FILE__,__LINE__);
    //setupTextures(shdr, "../assets/container_bumped.tga");
    
    // initialize the gbuffer
    gbuff = initGBuffer(scrn);
    // initialize assoicated framebuffer
    initFramebuffer(drawBuffers, 2, gbuff);
    checkGLFramebufferError(__FILE__, __LINE__);
    // set the uniforms
    setGbuffUniforms(gbuff, scrn.shader);
    // bind the outputs from the shdr to the gbuffer which will then be written to the scrn
    bindGbufferOutputs(gbuff, shdr);

    checkGLError("Before Setup VBO.",__FILE__,__LINE__);
    bffr = setupVBO(obj, shdr);
	program photonShader();
#ifdef DEBUG
    obsErrorMsg("Right before rendering!", __FILE__, __LINE__);
#endif
    while (_rendering && glfwGetWindowParam(GLFW_OPENED)) {
        render(shdr, bffr, gbuff, scrn);

#ifdef DEBUG
        obsErrorMsg("After render func.", __FILE__, __LINE__);
#endif

        drawScreen(shdr, bffr, scrn, gbuff);
		//drawPhotons(photonShdr, photons, scrn, gbuff);
#ifdef DEBUG
        obsErrorMsg("After drawScreen func.", __FILE__, __LINE__);
#endif

        glfwSwapBuffers();
#ifdef DEBUG
        obsErrorMsg("Bottom of render loop.", __FILE__, __LINE__);
#endif
		//obsPause();
    }

    destroyShader(shdr);
    destroyVBO(bffr);

    glfwTerminate();

    return EXIT_SUCCESS;
}
