#include <fstream>
#include <iostream>
#include <string>
#include "gshader.h"
using namespace std;

void readFile(const char *fname, string &data) {
	char buffer[2] = " ";
	ifstream file(fname);
	while (file.good()) {
		file.get(buffer[0]);
		data+=buffer;
	}
	file.close();
}

shader::shader(const char *fname, GLuint type) {
	if (!fname) {
		object = 0;
		return;
	}
	string text;
	readFile(fname, text);
	text+="\n"; // hack to let you not have a blank newline at the end of the file
	object = glCreateShader(type);
	GLint lentmp = text.size();
	const char *stemp = text.c_str();
	glShaderSource(object,1,&stemp,&lentmp);
	glCompileShader(object);
	
	// did it compile?
	GLint length;
	// get the size of the error message
	glGetShaderiv(object,GL_INFO_LOG_LENGTH,&length);
	if (length>13) { // includes the null termination
		char *log = new char[length];
		glGetShaderInfoLog(object,length,&length,log);
		cout << fname << " failed to compile. \n\n" << log << endl;
		exit(EXIT_FAILURE);
	}
    std::cout << "compiled shader: " << fname << std::endl;
}

shader::~shader() {
	glDeleteShader(object);
}

GLuint makeProgram(GLuint vert, GLuint frag, GLuint geom, GLuint ctrl, GLuint eval) {
	GLuint object = glCreateProgram();
	if (vert)	glAttachShader(object,vert);
	if (frag)	glAttachShader(object,frag);
	if (geom)	glAttachShader(object,geom);
	if (ctrl)	glAttachShader(object,ctrl);
	if (eval)	glAttachShader(object,eval);
	glLinkProgram(object);
	// did it link?
	GLint length;
	// get the size of the error message
	glGetProgramiv(object,GL_INFO_LOG_LENGTH,&length);
	if (length>13) { // includes the null termination
		char *log = new char[length];
		glGetProgramInfoLog(object,length,&length,log);
		cout << "failed to link. \n\n" << log << endl;
		exit(EXIT_FAILURE);
	}
	// make sure that the result can be run
	glValidateProgram(object);
	// did it validate?
	glGetProgramiv(object,GL_INFO_LOG_LENGTH,&length);
	if (length>13) { // includes the null termination
		char *log = new char[length];
		glGetProgramInfoLog(object,length,&length,log);
		cout << "failed to validate. \n\n" << log << endl;
		
		exit(EXIT_FAILURE);
	}
	
	glUniform1i(glGetUniformLocation(object, "colorMap"), OB_COLOR_MAP);
	glUniform1i(glGetUniformLocation(object, "normalMap"), OB_NORMAL_MAP);
	glUniform1i(glGetUniformLocation(object, "specularMap"), OB_SPECULAR_MAP);
	glUniform1i(glGetUniformLocation(object, "displacementMap"), OB_DISPLACEMENT_MAP);
	
	return object;
}

program::program(GLuint vert, GLuint frag, GLuint geom, GLuint ctrl, GLuint eval) {
	object = makeProgram(vert, frag, geom, ctrl, eval);
}

program::program(const char *vert, const char *frag, const char *geom, const char *ctrl, const char *eval) {
	shader	vs(vert,GL_VERTEX_SHADER),
			fs(frag,GL_FRAGMENT_SHADER),
			gs(geom,GL_GEOMETRY_SHADER),
			cs(ctrl,GL_TESS_CONTROL_SHADER),
			es(eval,GL_TESS_EVALUATION_SHADER);
	object = makeProgram(*vs,*fs,*gs,*cs,*es);
}

program::~program() {
	glDeleteProgram(object);
}
