#ifndef _OBS_SHADER_H_
#define _OBS_SHADER_H_

#include <iostream>
#include <stdio.h>

#include "glibs.h"
#include "errors.h"

// struct to hold the shader information
struct ShaderInfo {
    GLuint shaderID, // shader program ID
        frag, // fragment shader ID
        vert, // vertex shader ID
        sampler, // texture uniform location
        tex, // texture ID
        modelUniform, // model matrix uniform location
        viewUniform, // view matrix uniform location
        projectionUniform; // projection matrix uniform location
};

// texture functions
//GLuint setupTextures(ShaderInfo &shdr);
#ifdef _MSC_VER
GLuint setupTextures(ShaderInfo &shdr, const char *text = "C:/Users/matt/arch-render/assets/container_diffuse.tga");
#else
GLuint setupTextures(ShaderInfo &shdr, const char *text = "../assets/container_diffuse.tga");
#endif

// shader functions
GLuint compileShader(const char *file, GLuint type);
ShaderInfo setupShaders(char *vert, char *frag);
void destroyShader(ShaderInfo &shdr);
void bindMVP(ShaderInfo &shdr);

// bind the shader texture
void bindShaderTexture(ShaderInfo &shdr);
#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
