#ifndef _OBS_RENDER_H_
#define _OBS_RENDER_H_

//// OpenGL/glfw includes
//#include <GL/glew.h>
//#include <GL/glfw.h>

// glm includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// standard includes
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// local includes
#include "parser.h"

#include "shader.h"
#include "errors.h"
#include "glibs.h"
#include "keycb.h"
#include "collision.h"
#include "vectype.h"
#include "light.h"
#include "jankscene.h"

// struct to hold buffer handles
struct BufferInfo {
    GLuint vao,
        vbo,
        ibo,
        ibo_size;
};

void init();

void destroyVBO(BufferInfo &bffr);

BufferInfo setupVBO(ObjFile &obj, ShaderInfo &shdr);

void render(ShaderInfo &shdr, BufferInfo &bffr);

void bindTexture(ShaderInfo &shdr);

#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
