#include "light.h"

GLuint light_vao  = 0
            , light_vbo = 0
            , light_shdr = 0
            , ray_vao   = 0
            , ray_vbo   = 0
            , ray_shdr  = 0;

void init_light_vis(obs_light *light, unsigned int num_lights = 1) {
    glGenVertexArrays(1, &light_vao);
    checkGLError("Couldn't gen vao for light.", __FILE__, __LINE__);
    glBindVertexArray(light_vao);
    checkGLError("Couldn't bind vao for light.", __FILE__, __LINE__);

    glEnableVertexAttribArray(0); // position
    glEnableVertexAttribArray(1); // color
    glEnableVertexAttribArray(2); // attenuation
    checkGLError("Couldn't enable attrib locations for light.", __FILE__, __LINE__);

    glGenBuffers(1, &light_vbo);
    checkGLError("Couldn't gen vbo for light.", __FILE__, __LINE__);
    glBindBuffer(GL_ARRAY_BUFFER, light_vbo);
    checkGLError("Couldn't bind vbo for light.", __FILE__, __LINE__);

    glBufferData(GL_ARRAY_BUFFER, sizeof(obs_light) * num_lights, (GLvoid*)(light), GL_STATIC_DRAW);
    checkGLError("failed to set VAO data.", __FILE__, __LINE__);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(*light), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(*light), light + sizeof(obs_point));
    glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(*light), light + 2 * sizeof(obs_point));
    checkGLError("Failed to specify VAO data attributes.", __FILE__, __LINE__);
}

void init_light_shader() {
    GLuint vert, frag;
    light_shdr = glCreateProgram();
    checkGLError("Failed to create program.", __FILE__, __LINE__);
    
    vert = compileShader("light.vert", GL_VERTEX_SHADER);
    frag = compileShader("light.frag", GL_FRAGMENT_SHADER);
    checkGLError("Failed to compile shaders.", __FILE__, __LINE__);

    glAttachShader(light_shdr, vert);
    glAttachShader(light_shdr, frag);
    checkGLError("Failed to attach shaders.", __FILE__, __LINE__);

    glLinkProgram(light_shdr);
    checkGLError("Failed to link shaders.", __FILE__, __LINE__);
    checkProgram(light_shdr, frag, vert);
}

void visualize_lights(obs_light *light, unsigned int num_lights) {
    if (light_vao == 0 || light_vbo == 0) {
        init_light_vis(light, num_lights);
    }
    if (light_shdr == 0) {
        init_light_shader();
    }
    glBindVertexArray(light_vao);
    checkGLError("Couldn't bind vao for light.", __FILE__, __LINE__);
    glBindBuffer(GL_ARRAY_BUFFER, light_vbo);
    checkGLError("Couldn't bind vbo for light.", __FILE__, __LINE__);

    glUseProgram(light_shdr);
    checkGLError("Error using light shader program.", __FILE__, __LINE__);

    glDrawArrays(GL_POINTS, 0, num_lights);
    checkGLError("Failed to draw lights.", __FILE__, __LINE__);
}

void init_rays_shader() {
    GLuint vert, frag;
    light_shdr = glCreateProgram();
    checkGLError("Failed to create program.", __FILE__, __LINE__);
    
    vert = compileShader("line.vert", GL_VERTEX_SHADER);
    frag = compileShader("line.frag", GL_FRAGMENT_SHADER);
    checkGLError("Failed to compile shaders.", __FILE__, __LINE__);

    glAttachShader(light_shdr, vert);
    glAttachShader(light_shdr, frag);
    checkGLError("Failed to attach shaders.", __FILE__, __LINE__);

    glLinkProgram(light_shdr);
    checkGLError("Failed to link shaders.", __FILE__, __LINE__);
    checkProgram(light_shdr, frag, vert);
}

void init_rays(obs_ray *ray, unsigned int num_rays = 1) {
    glGenVertexArrays(1, &ray_vao);
    checkGLError("Couldn't gen vao for rays.", __FILE__, __LINE__);
    glBindVertexArray(ray_vao);
    checkGLError("Couldn't bind vao for rays.", __FILE__, __LINE__);

    glEnableVertexAttribArray(0); // position
    glEnableVertexAttribArray(1); // color
    glEnableVertexAttribArray(2); // attenuation
    checkGLError("Couldn't enable attrib locations for rays.", __FILE__, __LINE__);

    glGenBuffers(1, &ray_vbo);
    checkGLError("Couldn't gen vbo for rays.", __FILE__, __LINE__);
    glBindBuffer(GL_ARRAY_BUFFER, ray_vbo);
    checkGLError("Couldn't bind vbo for rays.", __FILE__, __LINE__);

    glBufferData(GL_ARRAY_BUFFER, sizeof(obs_ray) * num_rays, (GLvoid*)(ray), GL_STATIC_DRAW);
    checkGLError("failed to set VAO data.", __FILE__, __LINE__);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(*ray), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(*ray), ray + sizeof(obs_point));
    glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(*ray), ray + 2 * sizeof(obs_point));
    checkGLError("Failed to specify VAO data attributes.", __FILE__, __LINE__);
}

void visualize_rays(obs_ray *rays, unsigned int num_rays, obs_ray_hit *bounces, unsigned int num_bounces) {
    if (ray_vao == 0 || ray_vbo == 0) {
        init_rays(rays, num_rays);
    }
    if (ray_shdr == 0) {
        init_rays_shader();
    }
    glBindVertexArray(ray_vao);
    checkGLError("Couldn't bind vao for rays.", __FILE__, __LINE__);
    glBindBuffer(GL_ARRAY_BUFFER, ray_vbo);
    checkGLError("Couldn't bind vbo for rays.", __FILE__, __LINE__);

    glUseProgram(ray_shdr);
    checkGLError("Error using rays shader program.", __FILE__, __LINE__);

    glDrawArrays(GL_POINTS, 0, num_rays);
    checkGLError("Failed to draw rays.", __FILE__, __LINE__);
}
