#version 330

in vec2 ex_UV;
uniform sampler2DRect gbuff_Color;
uniform sampler2DRect gbuff_Position;
uniform sampler2DRect gbuff_Specular;

const float photonSize = 30;
uniform int restrict = 1;
uniform int photonCount = 2;
uniform vec2 photonLocs[10];
uniform vec3 photonColors[10];
uniform vec3 ambient;

//layout(location = 0) out vec4 out_Color; // 3 channel color, 1 channel of normal (x)
layout(location = 0) out vec4 out_Color; // 3 channel color, 1 channel of normal (x)
// layout(location=1) out vec4 out_Pos; // 3 channel position, 1 channel normal (y, recalculate z assuming normalized normal vectors)
// layout(location=2) out vec4 out_Spec; // 4 channel specularity

void main(void) {
	vec3 surfaceColor = texture(gbuff_Color, gl_FragCoord.xy+0.5).xyz;//vec4(0.5, 0.5, 1.0, 1.0);//
	if (restrict == 1) {
		/*float power = max(0,(1-distance(vec2(1280/2.2,768/4),gl_FragCoord.xy+0.5)/photonSize));
		power = power + max(0,(1-distance(vec2(1280/2,768/3),gl_FragCoord.xy+0.5)/photonSize));*/
		vec3 power = ambient;
		for (int i = 0; i<photonCount; ++i) {
			power = power + photonColors[i]*max(0,(1-distance(photonLocs[i],gl_FragCoord.xy+0.5)/photonSize));
		}
		surfaceColor = power * surfaceColor;
		out_Color = vec4(surfaceColor, 1);
		return;
	}
	vec3 eyevec = vec3(0.0, 0.0, 1.0);
	vec3 lpos = vec3(0.0, 0.0, 2.0);
	float lAmbient = 0.5;

	vec3 lighting = lAmbient * surfaceColor;

	vec3 L = normalize(lpos-texture(gbuff_Position, gl_FragCoord.xy+0.5).xyz);
	vec3 N = normalize(texture(gbuff_Specular, gl_FragCoord.xy+0.5).xyz);
	float lambertTerm = dot(N,L);
	if (lambertTerm > 0.0) {
		lighting+=vec3(0.5,0.2,0.2)*surfaceColor*lambertTerm;
		vec3 R = reflect(-L, N);
		float specular = pow(max(dot(R, eyevec), 0.0),texture(gbuff_Specular, gl_FragCoord.xy+0.5).w);
		lighting+=vec3(1,1,1)*surfaceColor*specular*lambertTerm;
	}
    // pass through
   out_Color = vec4(lighting,1);
}
