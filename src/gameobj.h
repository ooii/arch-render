#ifndef _OBS_GAMEOBJ_H_
#define _OBS_GAMEOBJ_H_
#include "generalmatrix.h"

typedef Matrix<float, 4, 4> t_matrix;

template <unsigned int depth> class MatrixStack {
    public:
        MatrixStack() {
            this->_transform[_top] = t_matrix();
        }
        MatrixStack(t_matrix &mat) {
            this->_transform[_top] = t_matrix(mat);
        }
        inline t_matrix& top() {
            return this->_transform[_top];
        }
        inline void push(t_matrix &mat) {
            // copy the data from the matrix into the next item on the stack (transform parent)
            this->_transform[this->_top + 1] = t_matrix(this->_transform[this->_top++]);
            // multiply by the transform
            this->top() = this->top() * mat;
        }
        inline t_matrix& pop() {
            // decrement top and return the "popped" value
            return this->_transform[this->_top--];
        }
    private:
        unsigned int _top;
        t_matrix _transform[depth];
}

class GameObject {
    public:
        GameObject *_parent; // parent object
        float *_vertices; // 

        // public constructors
        GameObject() {
            this->_parent = NULL;
            this->_transform = t_matrix();
        }
        
        void transform(t_matrix &mat) {
            this->_transform = this->_transform * mat;
        }
    private:
        t_matrix _transform; // transform matrix

};
#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
