#include "jankscene.h"

obs_col_face getCollisionFace(const obs_mesh *mesh, unsigned int first_vert) {
    obs_col_face face;
    unsigned int clamped_vert = first_vert + 2 < mesh->faces_size ? first_vert : first_vert > 2 ? first_vert - 2 : 0;
    memcpy(&(face.a), &(mesh->verts[mesh->faces[first_vert]]), 3 * sizeof(float));
    memcpy(&(face.b), &(mesh->verts[mesh->faces[first_vert + 1]]), 3 * sizeof(float));
    memcpy(&(face.c), &(mesh->verts[mesh->faces[first_vert + 2]]), 3 * sizeof(float));

    return face;
}

obs_vec3 getFaceNormal(const obs_mesh *mesh, unsigned int first_vert) {
    obs_vec3 normal, ac, ab;
    obs_col_face face = getCollisionFace(mesh, first_vert);
    
    sub(&ab, &(face.b), &(face.a));
    sub(&ac, &(face.c), &(face.a));

    cross3(&normal, &ab, &ac);

    normalize(&normal, &normal);

    return normal;
}
