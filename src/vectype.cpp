#include "vectype.h"

void copyvec(obs_float3 *dest, const obs_float3 *src) {
    dest->x = src->x;
    dest->y = src->y;
    dest->z = src->z;
}

void normalize(obs_float3 *res, const obs_float3 *vec) {
    div_scalar(res, vec, sqrt(sqrMagnitude(vec)));
}

void add(obs_float3 *res, const obs_float3 *a, const obs_float3 *b) {
    res->x = a->x + b->x;
    res->y = a->y + b->y;
    res->z = a->z + b->z;
}

void adds(obs_float3 *res, const obs_float3 *a, const float b) {
    res->x = a->x + b;
    res->y = a->y + b;
    res->z = a->z + b;
}

void sub(obs_float3 *res, const obs_float3 *a, const obs_float3 *b) {
    res->x = a->x - b->x;
    res->y = a->y - b->y;
    res->z = a->z - b->z;
}

void mul(obs_float3 *res, const obs_float3 *a, const obs_float3 *b) {
    res->x = a->x * b->x;
    res->y = a->y * b->y;
    res->z = a->z * b->z;
}

void muls(obs_float3 *res, const obs_float3 *a, const float b) {
    res->x = a->x * b;
    res->y = a->y * b;
    res->z = a->z * b;
}

void div_vector(obs_float3 *res, const obs_float3 *a, const obs_float3 *b) {
    res->x = a->x / b->x;
    res->y = a->y / b->y;
    res->z = a->z / b->z;
}

void div_scalar(obs_float3 *res, const obs_float3 *a, const float b) {
    res->x = a->x / b;
    res->y = a->y / b;
    res->z = a->z / b;
}

float sum(const obs_float3 *vec) {
    return vec->x + vec->y + vec->z;
}

float sqrMagnitude(const obs_float3 *vec) {
    return dot3(vec, vec);
}

float dot3(const obs_vec3 *v1, const obs_vec3 *v2) {
    obs_vec3 tmp;
    mul(&tmp, v1, v2);
    return sum(&tmp);
}

void cross3(obs_vec3 *res, const obs_vec3 *v1, const obs_vec3 *v2) {
    res->x = v1->y * v2->z - v1->z * v2->y;
    res->y = v1->z * v2->x - v1->x * v2->z;
    res->z = v1->x * v2->y - v1->y * v2->x;
}
