#ifndef _CUBE_H_
#define _CUBE_H_

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include "generalmatrix.h"

// declaration of the cube class, which handles the transforms
class Cube {
    public:
        Cube();
        // translate operations
        void cubeTranslateX(float &amount);
        void cubeTranslateY(float &amount);
        void cubeTranslateZ(float &amount);
        void cubeTranslateXYZ(float &dx, float &dy, float &dz);
        void cubeTranslateXYZ(Vec4f &v);
        // rotation operations
        void cubeRotateH(float &amount);
        void cubeRotateP(float &amount);
        void cubeRotateR(float &amount);
        void cubeRotateHPR(float &dh, float &dr, float &dp);
        // scale operations
        void cubeScale(float &amount);
        // general transform
        void cubeTransformXYZHPRS(float &dx, float &dy, float &dz, 
                              float &dh, float &dr, float &dp, 
                              float &s);
        // reset to defaults
        void reset();
        // apply the transform and return a string representation
        std::string transform();
    private:
        t_matrix _t_matrix; // the transformation matrix
        t_matrix _q_matrix; // the alternate matrix using quaternions for rotations
        Vec4f verts[8]; // the vertices
        size_t num_verts; // the number of vertices
};

#endif
