#ifndef _OBS_VECTYPES_H_
#define _OBS_VECTYPES_H_

#include <math.h>
#include "glibs.h"

typedef struct obs_vec3 {
    GLfloat x, y, z;
} obs_float3;

typedef struct obs_vec4 {
    GLfloat x, y, z, w;
} obs_float4;

typedef struct obs_ray3 {
    obs_float3 origin, dir;
} obs_ray;

typedef obs_vec3 obs_point;

void copyvec(obs_float3 *dest, const obs_float3 *src);

void normalize(obs_float3 *res, const obs_float3 *vec);

void add(obs_float3 *res, const obs_float3 *a, const obs_float3 *b);
void adds(obs_float3 *res, const obs_float3 *a, const float b);

void sub(obs_float3 *res, const obs_float3 *a, const obs_float3 *b);

void mul(obs_float3 *res, const obs_float3 *a, const obs_float3 *b);
void muls(obs_float3 *res, const obs_float3 *a, const float b);

void div_vector(obs_float3 *res, const obs_float3 *a, const obs_float3 *b);
void div_scalar(obs_float3 *res, const obs_float3 *a, const float b);

float sum(const obs_float3 *vec);

float sqrMagnitude(const obs_float3 *vec);

float dot3(const obs_vec3 *v1, const obs_vec3 *v2);

void cross3(obs_vec3 *res, const obs_vec3 *v1, const obs_vec3 *v2);
#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
