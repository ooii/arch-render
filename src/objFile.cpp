#include "objFile.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
using namespace std;
objFile::objFile(const char *fname) {
	// open the file
	fstream file(fname);
	// go through the lines
	string line;
	while (file) {
		getline(file,line);
		if (line.size()==0) continue;
		// discard stuff we don't care about
		if (line[0] == '#') continue;	// comments
		if (line.length()==0) continue;	// blank lines
		if (line[0] == 's') continue;	// smothing group
		if (line[0] == 'g') continue;	// group name
		if (line[0] == 'o') continue;	// object name
		if (line[0] == 'u') continue;	// usemtl
		if (line[0] == 'm') continue;	// mtllib
		// get serious about extracting values
		stringstream ss (stringstream::in | stringstream::out);
		ss.write(line.c_str(),line.length());
		string type;
		ss >> type;
		if (type == "v") {	// Vertex
			float x,y,z;
			ss >> x >> y >> z;
			//cout << "(" << x << ", " << y << ", " << z << ") ";
			v.push_back(make_tuple(x,y,z));
		} else if (type == "vn") {	// normals
			float x,y,z;
			ss >> x >> y >> z;
			vn.push_back(make_tuple(x,y,z));
		} else if (type == "f") {	// faces
			vector<string> verts;
			string a;
			do {
				ss >> a;
				verts.push_back(a);
			} while (ss);
			verts.pop_back();
			for (unsigned j=0; j<verts.size(); ++j) {
				for (unsigned i = 0; i<verts[j].size(); ++i) {
					if (verts[j][i] == '/') verts[j][i] = ' ';
				}
			}
			for (unsigned j=0; j<verts.size(); ++j) {
				stringstream vert(stringstream::in | stringstream::out);
				vert.write(verts[j].c_str(),verts[j].length());
				unsigned vals[3] = {0,0,0};
				for (unsigned i = 0; i<3 && vert; ++i) {
					vert >> vals[i];
					--vals[i];
				}
				if (verts.size()==3) {
					f.push_back(make_tuple(vals[0],vals[1],vals[2]));
				} else if (verts.size()==4) {
					qf.push_back(make_tuple(vals[0],vals[1],vals[2]));
				} else if (verts.size()==16) {
					pf.push_back(make_tuple(vals[0],vals[1],vals[2]));
					//cout << 16 << endl;
				}
			}
		} else if (type == "vt") {	// 
			float u,v;
			ss >> u >> v;
			vt.push_back(make_tuple(u,v));
		} else if (line.size()>1) {
			cout << line << endl;
		}
	}
	createVboData();
}

void objFile::createVboData() {
	vboData = new Vertex[f.size()+qf.size()];
	ibData = new unsigned[f.size()+qf.size()];
	for (unsigned i = 0; i<(f.size()+qf.size()); ++i) {
		const tuple<unsigned, unsigned, unsigned> &ind = (i<f.size()?f[i]:qf[i-f.size()]);
		const tuple<float, float, float> &pos = v[get<0>(ind)];
		const tuple<float, float, float> &nrm = vn[get<2>(ind)];
		const tuple<float, float> &tex = vt[get<1>(ind)];
		Vertex &vert = vboData[i];
		vert.x = get<0>(pos);
		vert.y = get<1>(pos);
		vert.z = get<2>(pos);
		vert.nx= get<0>(nrm);
		vert.ny= get<1>(nrm);
		vert.nz= get<2>(nrm);
		vert.u = get<0>(tex);
		vert.v = get<1>(tex);
		ibData[i] = i;
	}
	patchData = new BicubicPatchCorner[pf.size()/4];
	ipData = new unsigned[pf.size()/4];
	cout << sizeof(BicubicPatchCorner)*(pf.size()/4) << endl;
	for (unsigned i = 0, j=0; i<pf.size(); i+=4, j++) {
		BicubicPatchCorner &patch = patchData[j];
		{	const tuple<unsigned, unsigned, unsigned> &ind = pf[i];
			const tuple<float, float, float> &pos = v[get<0>(ind)];
			const tuple<float, float> &tex = vt[get<1>(ind)];
			patch.uv[0] = get<0>(tex);
			patch.uv[1] = get<1>(tex);
			
			patch.p0[0] = get<0>(pos);
			patch.p0[1] = get<1>(pos);
			patch.p0[2] = get<2>(pos);
		} {
			const tuple<unsigned, unsigned, unsigned> &ind = pf[i+1];
			const tuple<float, float, float> &pos = v[get<0>(ind)];
			patch.p1[0] = get<0>(pos);
			patch.p1[1] = get<1>(pos);
			patch.p1[2] = get<2>(pos);
		} {
			const tuple<unsigned, unsigned, unsigned> &ind = pf[i+2];
			const tuple<float, float, float> &pos = v[get<0>(ind)];
			patch.p2[0] = get<0>(pos);
			patch.p2[1] = get<1>(pos);
			patch.p2[2] = get<2>(pos);
		} {
			const tuple<unsigned, unsigned, unsigned> &ind = pf[i+3];
			const tuple<float, float, float> &pos = v[get<0>(ind)];
			patch.p3[0] = get<0>(pos);
			patch.p3[1] = get<1>(pos);
			patch.p3[2] = get<2>(pos);
		}
		ipData[j] = j;
	}
}

objFile::~objFile() {
	delete[] vboData;
	delete[] ibData;
}