#include <iostream>
#include <FreeImage.h>
#include <stdlib.h>
#include "glibs.h"
#include "texture.h"
using namespace std;

class FreeImage2D {
public:
	FreeImage2D(const char *fname) {
		// detect the format from the file
		FREE_IMAGE_FORMAT format;
		format = FreeImage_GetFileType(fname, 0);
		// guess from filename if we couldn't figure it out
		if(format == FIF_UNKNOWN) {
			format = FreeImage_GetFIFFromFilename(fname);
			// fail spectacularly if all else failes
			if(format == FIF_UNKNOWN) {
				cout << "ERROR LOADING '" << fname << "'\nCould not detect format" << endl;
				exit(EXIT_FAILURE);
			}
		}
		// make sure that we can read it
		if(!FreeImage_FIFSupportsReading(format)) {
			cout << "ERROR LOADING '" << fname << "'\nFormat not supported" << endl;
			exit(EXIT_FAILURE);
		}
		// read it
		raw = FreeImage_Load(format, fname);
		// make sure that we read it properly
		if(!raw) {
			cout << "ERROR LOADING '" << fname << "'\nPossibly corrupt file" << endl;
			exit(EXIT_FAILURE);
		}
		// get the image from the raw data
		bData = FreeImage_GetBits(raw);
		// get structural data
		iWidth = FreeImage_GetWidth(raw); // Get the image width and height
		iHeight = FreeImage_GetHeight(raw);
		iBPP = FreeImage_GetBPP(raw);
		// make sure everything worked.. this should never fail but would be a bitch to debug
		// if it somehow did and we let it past this point
		if(bData == NULL || iWidth == 0 || iHeight == 0 || iBPP == 0) {
			cout << "ERROR LOADING '" << fname << "'\nInternal Error: Error after successful read. This should never happen on functional hardware." << endl;
			if (bData == NULL) cout << "Could not get pixels from raw data" << endl;
			if (iWidth == 0 || iHeight == 0) cout << "Zero image dimension" << endl;
			if (iBPP == 0 ) cout << "Zero bits per pixel" << endl;
			exit(EXIT_FAILURE);
		}
	}
	inline BYTE* operator*() { return bData; }
	inline BYTE* data() { return bData; }
	inline GLuint width() const { return iWidth; }
	inline GLuint height() const { return iHeight; }
	inline GLuint bpp() const { return iBPP; }
	~FreeImage2D() {
		FreeImage_Unload(raw);
	}
private:
	bool initialized;
	// attributes
	GLuint iWidth, iHeight, iBPP;
	// raw data
	FIBITMAP* raw;
	BYTE* bData;
};

Texture2D::Texture2D(const char *fname) {
	bHasMipmap=false;
	
	FreeImage2D data(fname);
	// Generate an OpenGL texture ID for this texture
	//glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &uTexture);
	glBindTexture(GL_TEXTURE_2D, uTexture);
	// translate from BPP to OpenGL format
	int iFormat = data.bpp()==32 ? GL_BGRA : data.bpp() == 24 ? GL_BGR : data.bpp() == 8 ? GL_LUMINANCE : 0;
	// we may need to program more of these in
	if (iFormat == 0) {
		cout << "ERROR LOADING '" << fname << "'\nUnrecognized BPP = " << data.bpp() << endl;
		exit(EXIT_FAILURE);
	}
	cout << "width: " << data.width() << endl;
	cout << "height: " << data.height() << endl;
	int levels = 0;
	for (int i = data.width(); i>0; i = i/2) { levels++; }
	// send data to OpenGL
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, levels);
	//glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB, data.width(), data.height());
	//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, data.width(), data.height(), iFormat, GL_UNSIGNED_BYTE, *data);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, data.width(), data.height(), 0, iFormat, GL_UNSIGNED_BYTE, *data);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameterf(uTexture,GL_TEXTURE_MAX_ANISOTROPY_EXT,16.0f);
	glTexParameterf(uTexture,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
}

Texture2D::~Texture2D() {
	cout << "NoOOO" << endl;
	glDeleteTextures(1,&uTexture);
}
