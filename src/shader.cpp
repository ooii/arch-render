#include "shader.h"

// set up a texture
GLuint setupTextures(ShaderInfo &shdr, const char *text) {
    // generate the texture
	checkGLError("Enter setup texture.",__FILE__,__LINE__);
    glGenTextures(1, &(shdr.tex));
	checkGLError("Failed to generate texture.",__FILE__,__LINE__);
    // bind texture
    glBindTexture(GL_TEXTURE_2D, shdr.tex);
	checkGLError("Failed to bind texture.",__FILE__,__LINE__);

    // set linear filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // set UVs to repeat when wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	checkGLError("Failed to set texture parameters",__FILE__,__LINE__);
    // cheats and loads texture using glfw
    if (!glfwLoadTexture2D(text, 0)) {
        std::cerr << __FILE__ << ":" << __LINE__ << " Failed to load texture." << std::endl;
    }
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
	glTexEnvf(GL_TEXTURE_FILTER_CONTROL, GL_TEXTURE_LOD_BIAS, 0);
	checkGLError("Failed to load texture.",__FILE__,__LINE__);
    // sets the sampler uniform to the proper location (0)
	glUseProgram(shdr.shaderID);
	checkGLError("Failed to bind program.",__FILE__,__LINE__);
    glUniform1i(shdr.sampler, 0);
	checkGLError("Failed to bind texture.",__FILE__,__LINE__);
    return shdr.tex;
}

GLuint compileShader(const char *file, GLuint type) {
    int f_len = 0; // file length

    // attempt to open the file for reading
    FILE *fp = fopen(file, "r");
    if (!fp) {
std::cerr << "Failed to open file for reading: " << std::string(file) << std::endl;
        exit(EXIT_FAILURE);
    }
    // reset the file stream
    rewind(fp);
    
    // create the shader with the given type
    GLuint shader = glCreateShader(type);
    
    // bleh, file IO.  i have no clue how to use fstreams properly, sooooo
    // seek to the end
    if (fseek(fp, 0, SEEK_END)) {
        std::cerr << "Could not find EOF in file: " << std::string(file) << std::endl;
        exit(EXIT_FAILURE);
    }
    // get the length
    f_len = ftell(fp);
    if (f_len <= 0) {
        std::cerr << "Could not determine length of file: " << std::string(file) << std::endl;
        exit(EXIT_FAILURE);
    }
    // reset the file stream
    rewind(fp);
#ifdef DEBUG
    std::cout << "File length of " << std::string(file) << ": " << f_len << std::endl;
#endif
    // allocate f_len bytes +1 for null character
    //GLchar tmp[f_len + 1]; // better for gcc
	GLchar *tmp = new GLchar[f_len + 1];
	GLuint bb = fread(tmp, sizeof(GLchar), f_len, fp);
    if (bb < f_len/2) {
        std::cerr << "Could not read file: " << std::string(file) << std::endl;
		std::cout << f_len << " " << bb << std::endl;
		std::cout << tmp << std::endl;
        exit(EXIT_FAILURE);
    }
    // ensure the file string is null terminated
    tmp[bb] = '\0';
#ifdef DEBUG
    std::cout << "Shader " << std::string(file) << ": \n\n" << std::string(tmp) << std::endl;
#endif
    const GLchar *src = tmp;

    // set the shader source
    glShaderSource(shader, 1, &src, &f_len);

    // compile the shader
    glCompileShader(shader);
    checkGLError("Shader compilation issue.", __FILE__, __LINE__);
    checkShader(shader, file);
	delete[] tmp;
    return shader;
}

ShaderInfo setupShaders(char *vert, char *frag) {
    ShaderInfo shdr;
    shdr.shaderID = glCreateProgram();
    checkGLError("Failed to create shader program.", __FILE__, __LINE__);

    // bind the attribute location for the position and uv coordinate
    glBindAttribLocation(shdr.shaderID, 0, "in_Pos");
    glBindAttribLocation(shdr.shaderID, 1, "in_UV");

    // read and compile the vertex/fragment shaders
    shdr.vert = compileShader(vert, GL_VERTEX_SHADER);
    shdr.frag = compileShader(frag, GL_FRAGMENT_SHADER);

    // attach the shaders to the program
    glAttachShader(shdr.shaderID, shdr.vert);
    glAttachShader(shdr.shaderID, shdr.frag);

    // link the shaders
    glLinkProgram(shdr.shaderID);
	GLint linkStats;
	glGetProgramiv(shdr.shaderID,GL_LINK_STATUS,&linkStats);
	printf("Shader %i linked %s\n",shdr.shaderID,(linkStats==GL_TRUE?"successfully":"unsuccessfully"));
    checkGLError("Failed to link shader program.", __FILE__, __LINE__);

    return shdr;
}

void bindMVP(ShaderInfo &shdr) {
    checkGLError("Top of bindMVP", __FILE__, __LINE__);
    glUseProgram(shdr.shaderID);
    checkGLError("Failed to use shader program.", __FILE__, __LINE__);

    // get the uniform locations if i used these
    shdr.sampler = glGetUniformLocation(shdr.shaderID, "in_Tex");
    checkGLError("Failed to get sampler location.", __FILE__, __LINE__);
    shdr.modelUniform = glGetUniformLocation(shdr.shaderID, "model");
    checkGLError("Failed to get model location.", __FILE__, __LINE__);
    shdr.viewUniform = glGetUniformLocation(shdr.shaderID, "view");
    checkGLError("Failed to get view location.", __FILE__, __LINE__);
    shdr.projectionUniform = glGetUniformLocation(shdr.shaderID, "projection");
    checkGLError("Failed to get projection location.", __FILE__, __LINE__);
}

void destroyShader(ShaderInfo &shdr) {
    // detach the shaders from the program
    glDetachShader(shdr.shaderID, shdr.vert);
    glDetachShader(shdr.shaderID, shdr.frag);
    
    // delete the shaders
    glDeleteShader(shdr.vert);
    glDeleteShader(shdr.frag);

    // delete the shader program
    glDeleteProgram(shdr.shaderID);
    checkGLError("Could not destroy shader.", __FILE__, __LINE__);
}

// bind the shader texture
void bindShaderTexture(ShaderInfo &shdr) {
    glUniform1i(shdr.sampler, 4);
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, shdr.sampler);
}
