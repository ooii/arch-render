#version 330

in vec2 ex_UV;
in vec3 ex_Nrm;
in vec3 ex_Pos;
uniform sampler2D in_Tex;

out vec4 out_Pos;//layout(location = 2) 
out vec4 out_Spec;//layout(location = 1) 
out vec4 out_Color;//layout(location = 0) 

uniform mat4 model;
uniform mat4 view;
void main(void) {
    out_Color = texture(in_Tex, ex_UV);
	out_Pos = vec4(ex_Pos,1);
	out_Spec = vec4(normalize(mat3(transpose(inverse(view*model))) * ex_Nrm), 10.0);
    //out_Color = vec4(0.5, 0.5, 0.5, 0.5);
}
