#ifndef __gshader_h__
#define __gshader_h__
#include "glibs.h"
/*  expected in-shader names for different maps...
colorMap, normalMap, specularMap, displacementMap
*/
enum {
	OB_COLOR_MAP = 0,
	OB_NORMAL_MAP = 1,
	OB_SPECULAR_MAP = 2,
	OB_DISPLACEMENT_MAP = 3,
	OB_MAP_COUNT = 4
};
class shader {
private:
	GLuint object;
public:
	shader(const char *fname, GLuint type);
	~shader();
	GLuint operator*() {
		return object;
	}
};

class program {
private:
	GLuint object;
public:
	program(GLuint vert, GLuint frag, GLuint geom=0, GLuint ctrl=0, GLuint eval=0);
	program(const char *vert, const char *frag, const char *geom=0, const char *ctrl=0, const char *eval=0);
	~program();
	GLuint operator*() {
		return object;
	}
};

#endif
