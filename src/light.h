#ifndef _OBS_LIGHT_
#define _OBS_LIGHT_

#include "vectype.h"
#include "errors.h"
#include "collision.h"
#include "shader.h"

/*! Point let definition. */
typedef struct obs_point_light {
    obs_point pos; //!< Position (center) of the light
    obs_vec3 color; //!< Color of the light
    float range; //!< Range of the light
} obs_light;

struct obs_photon_hit {
    obs_point pos; //!< Position of the photon
    obs_vec3 color; //!< Color of the photon
    float intensity; //!< Intensity of the photon
};

/*! Draw something for the light (small triangle, no culling). */
void visualize_lights(obs_light *light, unsigned int num_lights = 1);

/*! Draw lines for the rays (green ray, blue bounces?) */
void visualize_rays(obs_ray *rays, unsigned int num_rays, obs_ray_hit *bounces, unsigned int num_bounces);

#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
