#ifndef _OBS_JANK_SCENEGRAPH_
#define _OBS_JANK_SCENEGRAPH_

#include <list>
#include <stdlib.h>

#include "glibs.h"
#include "collision.h"
#include "parser.h"

struct obs_mesh_collider;
typedef obs_mesh_collider obs_mcol;
struct obs_tri;
typedef obs_tri obs_col_face;

/*! Class to hold a game object. */
class obs_game_obj {
    public:
        obs_mesh *mesh; //!< Pointer to the object's mesh.
        //obs_mesh_collider collider; //!< The mesh collider.
};

/*! Get a collision face from a mesh face (necessary because of indexed vertices and also poor planning). */
obs_col_face getCollisionFace(const obs_mesh *mesh, unsigned int first_vert);

/*! Get the normal for face from a mesh (again, because of indexed verts). */
obs_vec3 getFaceNormal(const obs_mesh *mesh, unsigned int first_vert);

/*! Scene object. */
class obs_scene {
    public: 
        std::list<obs_game_obj> objects;
        //std::list<obs_vec3> obs
};
#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
