#version 330

in vec3 in_Pos;
in vec2 in_UV;
in vec3 in_Nrm;
out vec2 ex_UV;
out vec3 ex_Nrm;
out vec3 ex_Pos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(void) {
    // pass the position through modified by the transforms
    gl_Position = (projection * view * model) * vec4(in_Pos,1);
	ex_Nrm = in_Nrm;
    // pass the texture coordinate through
    ex_UV = in_UV;
	ex_Pos = in_Pos;
}
