#include "collision.h"

obs_vec3 computeCentroid(const obs_vec3 *data, const unsigned int nmemb) {
    unsigned int i;
    obs_vec3 tmp;

    for (i = 0; i < nmemb; ++i) {
        add(&tmp, &tmp, &(data[i]));
    }
    div_scalar(&tmp, &tmp, (float)nmemb);

    return tmp;
}

obs_vec3 meshCentroid(const obs_mesh *data) {
    obs_vec3 tmp;
    memset(&tmp, 0, sizeof(tmp));

    for (int i = 0; i < data->verts_size; ++i) {
        tmp.x += data->verts[i].pos[0];
        tmp.y += data->verts[i].pos[1];
        tmp.z += data->verts[i].pos[2];
    }

    div_scalar(&tmp, &tmp, (float)data->verts_size);
    return tmp;
}

obs_ray castRay(const obs_vec3 *origin, const obs_vec3 *dir) {
    obs_ray ray;
    ray.origin = *origin;
    normalize(&(ray.dir), dir);
    return ray;
}

//int rayToMesh(const obs_ray *ray, const obs_mesh_collider *mesh, obs_ray_hit *hit) {
int rayToMesh(const obs_ray *ray, const obs_mesh *mesh, obs_ray_hit *hit) {
    unsigned int i;
    int col_ind = -1;
    obs_ray_hit tmp;
    obs_col_face face;
    // initialize the hit as the first element
    //if (rayToTri(ray, &(mesh->faces[0]), hit)) col_ind = 0;
    face = getCollisionFace(mesh, 0);
    if (rayToTri(ray, &face, hit)) col_ind = 0;
    //for (i = 0; i < mesh->num_faces; ++i) {
    for (i = 0; i < mesh->faces_size; i += 3) {
        face = getCollisionFace(mesh, i);
        if (rayToTri(ray, &face, &tmp)) {
            if (tmp.line_param < hit->line_param) {
                *hit = tmp;
            }
        }
    }
    return col_ind;
}

/*! Collide a ray to a triangle. */
unsigned int rayToTri(const obs_ray *ray, const obs_col_face *face, obs_ray_hit *hit) {
    obs_float3 barycent;
    obs_vec3 ab;
    obs_vec3 ac;
    sub(&ab, &(face->b), &(face->a));
    sub(&ac, &(face->c), &(face->a));
    // normal to triangle plane is face->normal
    obs_vec3 normal;
    cross3(&normal, &ab, &ac);
    // compute denominator d (dir vector dot normal)
    //      exit if less than 0 (facing away)
    float d = dot3(&(ray->dir), &(normal));
    if (d <= 0.0f) return 0;
    // compute t value of ray intersecting with plane of triangle
    //      if t is negative, return (failed, should be facing the same way-ish)
    obs_vec3 ap;
    sub(&ap, &(ray->origin), &(face->a));
    hit->line_param = dot3(&ap, &normal);
    if (hit->line_param < 0.0f) return 0;
    // compute vector e (perpendicular to ray and line from origin to plane)
    obs_vec3 e;
    cross3(&e, &(ray->dir), &ap);
    // compute components v and w (y and z), u (x) will be 1 - v - w
    barycent.y = dot3(&ac, &e);
    if (barycent.y < 0.0f || barycent.y > d) return 0;
    barycent.z = -1 * dot3(&ab, &e);
    if (barycent.z < 0.0f || barycent.y + barycent.z > d) return 0;

    // it intersects! finish calculation
    float inv_d = 1.0f / d;
    hit->line_param *= inv_d;
    barycent.y *= inv_d;
    barycent.z *= inv_d;
    barycent.x = 1.0f - barycent.y - barycent.z;
    obs_vec3 tmp;
    muls(&(hit->point), &(face->a), barycent.x);
    muls(&tmp, &(face->b), barycent.y);
    add(&(hit->point), &(hit->point), &tmp);
    muls(&(hit->point), &(face->c), barycent.z);
    add(&(hit->point), &(hit->point), &tmp);
    return 1;
}

unsigned int rayToSphere(const obs_ray ray, const obs_sphere_collider sphere, obs_ray_hit *hit) {
    // vector between ray origin and sphere center
    obs_vec3 m;
    sub(&m, &(ray.origin), &(sphere.center));
    // angle between ray direction and vector from ray to sphere center
    float b = dot3(&m, &(ray.dir));
    // difference between sphere radius and the vector between ray origin and center
    float c = dot3(&m, &m) - (sphere.radius * sphere.radius);
    
    // no intersection if ray originates outside sphere and is pointing away from it
    if (c > 0.0f && b > 0.0f) return 0;
    float discr = b * b - c;

    // negative discriminant means it missed
    if (discr < 0.0f) return 0;

    // intersecting ray, find t value
    hit->line_param = -1 * b - sqrt(discr);
    // if negative, started inside sphere, so clamp at 0
    if (hit->line_param < 0.0f) hit->line_param = 0.0f;
    muls(&(hit->point), &(ray.dir), hit->line_param);
    add(&(hit->point), &(ray.origin), &(hit->point));
    return 1;
}

/*! Reflect a ray across a normal. */
float reflectRay(obs_ray *new_ray, const obs_ray *orig, const obs_vec3 *surf_norm) {
    float nd = dot3(surf_norm, &(orig->dir));
    muls(&(new_ray->dir), surf_norm, 2.f * nd);
    sub(&(new_ray->dir), &(orig->dir), &(new_ray->dir));
    return nd;
}

unsigned int bounceRay(const obs_ray *ray, const unsigned int num_bounces, obs_ray_hit *bounces, obs_scene *scene) {
    obs_ray bounced = *ray; // the current ray that we're tracing
    // we need a real number of bounces...c'mon man
    if (num_bounces < 1) {
        return 0;
    }

    // we also need things to collide with in the scene.  can't very well light the emptiness of space
    if (scene->objects.size() > 0) {
        rayToMesh(&bounced, scene->objects.begin()->mesh, &(bounces[0]));
    }
    else {
        return 0;
    }

    for (unsigned int i = 0; i < num_bounces; ++i) {
        bool hit = false;
        for (std::list<obs_game_obj>::iterator itr = scene->objects.begin(); itr != scene->objects.end(); ++itr) {
            obs_ray_hit tmp;
            int tri_ind = rayToMesh(&bounced, itr->mesh, &tmp);
            if (tri_ind > 0) {
                if (!hit || tmp.line_param < bounces[i].line_param) {
                    bounces[i] = tmp;
                    obs_vec3 norm = getFaceNormal(itr->mesh, tri_ind);
                    reflectRay(&bounced, &bounced, &norm);
                    bounced.origin = bounces[i].point;
                }
                hit = true;
            }
        }
    }
    return 0;
}
