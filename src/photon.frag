#version 330 core

uniform sampler2DRect gbuff_Color;
uniform sampler2DRect gbuff_Position;
uniform sampler2DRect gbuff_Specular;

in vec4 gcolor;
out vec4 color;

void main(void) {
	color = gcolor;
}
