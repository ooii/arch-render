#ifndef _OBS_DEFERRED_RENDER_H_
#define _OBS_DEFERRED_RENDER_H_

// we're going to need 3 textures
// --uv
// normal (store x and y, 1 each in color and position and recalculate z)
// specular (colored) (need 3 for color, 1 for exponent)
// color (3 channels)
// position (3 channels)

// render pass with depth test culling with gl_less, 

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "glibs.h"
#include "texture.h"
//#include "parser.h"
#include "objFile.hpp"
#include "errors.h"
#include "shader.h"
#include "keycb.h"

// glm includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

struct Screen {
    GLuint width, height;
    GLuint vao, vbo;
    ShaderInfo shader;
};

// struct to hold buffer handles
struct BufferInfo {
    GLuint vao,
        vbo,
        ibo,
        ibo_size;
};

struct GBuffer {
    GLuint pos, // frag output 1 (position + 1 channel of normal)
           specular, // frag output 2 (specular)
           color, // frag output 0 (color + 1 channel of normal)
           depth, // depth buffer is this necessary? i think so?
           gbuff, // framebuffer for the gbuffer
           pos_sampler, // sampler for position texture
           spec_sampler, // sampler for spec map
           col_sampler; // sampler for color texture
};

// gbuffer functions
GBuffer initGBuffer(Screen &scrn);
void initGBufferTexture(GLuint *tex, Screen &scrn);
void setGbuffUniforms(GBuffer &gbuff, ShaderInfo &shdr);
void initFramebuffer(GLenum *drawBuffers, const unsigned int size, GBuffer gbuff);
void bindGbufferOutputs(ShaderInfo &shdr);

// VBO functions
BufferInfo setupVBO(objFile &obj, ShaderInfo &shdr);
void destroyVBO(BufferInfo &bffr);

// initialization function
void init(int WindowWidth, int WindowHeight);

GBuffer initGBuffer(Screen &scrn) {
    // the gbuffer we're initializing
    GBuffer buff;

    // framebuffer for the gbuffer
    glGenFramebuffers(1, &(buff.gbuff));
    checkGLError("Failed to generate frame buffer.", __FILE__, __LINE__);
    glBindFramebuffer(GL_FRAMEBUFFER, buff.gbuff);
    checkGLError("Failed to bind frame buffer.", __FILE__, __LINE__);
    // generate the textures
    {
        GLuint tex[3];
        glGenTextures(3, tex);
        checkGLError("Failed to generate textures for Gbuffer.", __FILE__, __LINE__);
        buff.pos = tex[0];
        buff.specular = tex[1];
        buff.color = tex[2];
    }
    // initialize the textures
    initGBufferTexture(&(buff.pos), scrn);
	glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_RECTANGLE,buff.pos,0);
	checkGLError("Failed to bind texture to framebuffer.", __FILE__, __LINE__);
    initGBufferTexture(&(buff.specular), scrn);
	glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT1,GL_TEXTURE_RECTANGLE,buff.pos,0);
	checkGLError("Failed to bind texture to framebuffer.", __FILE__, __LINE__);
    initGBufferTexture(&(buff.color), scrn);
	glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT2,GL_TEXTURE_RECTANGLE,buff.pos,0);
	checkGLError("Failed to bind texture to framebuffer.", __FILE__, __LINE__);
	printf("Textures bound to gbuffer\n");

    // generate depth buffer
    glGenRenderbuffers(1, &(buff.depth));
    // set up render buffer to take depth render data
    glBindRenderbuffer(GL_RENDERBUFFER, buff.depth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, scrn.width, scrn.height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, buff.depth);
	checkGLError("Failed to bind renderbuffer to framebuffer.", __FILE__, __LINE__);

	const GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
	glDrawBuffers(3, buffers);
	/*GLint oname;
	glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,&oname);
	printf("Depth component %i==%i?\n",buff.depth,oname);*/
    // return the completed gbuffer
    return buff;
}

void initFramebuffer(GLenum *drawBuffers, const unsigned int size, GBuffer gbuff) {
    // bind the framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, gbuff.gbuff);
    checkGLError("Failed to bind framebuffer", __FILE__, __LINE__);
    // set up the frame buffer
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gbuff.color, 0);
    checkGLError("Failed to set framebuffer texture", __FILE__, __LINE__);
    
    // initialize drawBuffers
    glDrawBuffers(3, drawBuffers);
    checkGLError("Fuck your drawbuffers i don' need dat shit.", __FILE__, __LINE__);
}

void initGBufferTexture(GLuint *tex, Screen &scrn) {
    // the last texture bound
    GLint prev = 0;
    // glGet to get the current bound texture
    // GL_TEXTURE_BINDING_RECTANGLE is current texture bound to GL_TEXTURE_RECTANGLE, 0 is default
    // GL_ACTIVE_TEXTURE gives the current active texture from glActiveTexture()
    glGetIntegerv(GL_TEXTURE_BINDING_RECTANGLE, &prev);
    checkGLError("Failed getintegerv for initializing texture.", __FILE__, __LINE__);

    // bind the texture
    glBindTexture(GL_TEXTURE_RECTANGLE, *tex);
    checkGLError("Failed binding texture.", __FILE__, __LINE__);

    // fill with an empty texture
    // target, level, internal type, width, height, border, format, type, data; 0 for data means empty texture
    glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA, scrn.width, scrn.height, 0, GL_RGBA, GL_FLOAT, 0);
    checkGLError("Failed generating texture.", __FILE__, __LINE__);
    // set filtering
    glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    checkGLError("Failed setting tex parameter.", __FILE__, __LINE__);
    glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    checkGLError("Failed setting tex parameter.", __FILE__, __LINE__);

    // unbind the texture
    glBindTexture(GL_TEXTURE_RECTANGLE, prev);
    checkGLError("Failed rebinding old texture.", __FILE__, __LINE__);
}

void bindGbufferOutputs(const GBuffer gbuff, const ShaderInfo &shdr) {
    glBindFramebuffer(GL_FRAMEBUFFER, gbuff.gbuff);
    // color in frag location 0
    glBindFragDataLocation(shdr.shaderID, 0, "out_Color");
    checkGLError("Failed to bind frag data location (color).", __FILE__, __LINE__);

    // position in frag location 1
    glBindFragDataLocation(shdr.shaderID, 1, "out_Pos");
    checkGLError("Failed to bind frag data location (position).", __FILE__, __LINE__);

    // specular in frag location 2
    glBindFragDataLocation(shdr.shaderID, 2, "out_Spec");
    checkGLError("Failed to bind frag data location (specular).", __FILE__, __LINE__);
}
void setGbuffUniforms(GBuffer &gbuff, ShaderInfo &shdr) {
    checkGLError("top of setGbuffUniforms", __FILE__, __LINE__);
    glBindFramebuffer(GL_FRAMEBUFFER, gbuff.gbuff);
    checkGLError("Binding framebuffer...", __FILE__, __LINE__);
	checkGLFramebufferError(__FILE__, __LINE__);

    glUseProgram(shdr.shaderID);
    checkGLProgramError(shdr, __FILE__, __LINE__);
    gbuff.col_sampler = glGetUniformLocation(shdr.shaderID, "gbuff_Color");
    checkGLError("Failed to get color sampler location.", __FILE__, __LINE__);
    gbuff.pos_sampler = glGetUniformLocation(shdr.shaderID, "gbuff_Pos");
    checkGLError("Failed to get position sampler location.", __FILE__, __LINE__);
    gbuff.spec_sampler = glGetUniformLocation(shdr.shaderID, "gbuff_Spec");
    checkGLError("Failed to get specular location.", __FILE__, __LINE__);
}

void bindGbuffTextures(GBuffer &gbuff) {
    glUniform1i(gbuff.col_sampler, 0);
	checkGLError("Error binding color sampler.", __FILE__, __LINE__);
    glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuff.color);
	checkGLError("Error binding color texture.", __FILE__, __LINE__);

    glUniform1i(gbuff.pos_sampler, 1);
	checkGLError("Error binding position sampler.", __FILE__, __LINE__);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_RECTANGLE, gbuff.pos);
	checkGLError("Error binding position texture.", __FILE__, __LINE__);

    glUniform1i(gbuff.spec_sampler, 2);
	checkGLError("Error binding spec sampler.", __FILE__, __LINE__);
    glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuff.specular);
	checkGLError("Error binding spec texture.", __FILE__, __LINE__);
}

#else
#ifdef DEBUG
#warning Multiple definitions of __FILE__.
#endif
#endif
