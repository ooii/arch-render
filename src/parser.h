#ifndef _OBS_PARSER_H_
#define _OBS_PARSER_H_

#include <fstream>
#include <string>
#include <stdio.h>
#include <iostream>

typedef struct Vertex {
    float pos[4];
    float uv[2];
} obs_vert;

typedef struct ObjFile {
    Vertex *verts;
    unsigned int *faces;
    size_t verts_size
         , faces_size;
} obs_mesh;

#ifdef TESTING
void printTest(ObjFile result);
#endif

#ifdef DEBUG
void printResult(ObjFile result);
#endif

ObjFile parseFile(std::string fname);
#endif
