#include <string>
#include <iostream>
#include <sstream>
#include <math.h>
#include "matrix.h"
#include "quaternion.h"

Cube::Cube() {
    this->reset();
    this->num_verts = 8;
}

void Cube::reset() {
    this->verts[0] = Vector3<float>(1.0f, 1.0f, 1.0f);
    this->verts[1] = Vector3<float>(1.0f, 1.0f, -1.0f);
    this->verts[2] = Vector3<float>(1.0f, -1.0f, 1.0f); 
    this->verts[3] = Vector3<float>(1.0f, -1.0f, -1.0f);
    this->verts[4] = Vector3<float>(-1.0f, 1.0f, 1.0f);
    this->verts[5] = Vector3<float>(-1.0f, 1.0f, -1.0f);
    this->verts[6] = Vector3<float>(-1.0f, -1.0f, 1.0f);
    this->verts[7] = Vector3<float>(-1.0f, -1.0f, -1.0f);
    const static float temp[] = {1.0f, 0.0f, 0.0f, 0.0f, 
                                 0.0f, 1.0f, 0.0f, 0.0f, 
                                 0.0f, 0.0f, 1.0f, 0.0f, 
                                 0.0f, 0.0f, 0.0f, 1.0f};
    this->_t_matrix = Matrix<float>(temp);
    this->_q_matrix = Matrix<float>(temp);
}

std::string Cube::transform() {
    std::string temp = "";
    for (int i = 0; i < this->num_verts; i++) {
        //this->verts[i] = this->_t_matrix * this->verts[i];
        temp += "Normal Transform: \t" + (this->_t_matrix * this->verts[i]).stringify()
             + "\nQuaternion Transform: \t" + (this->_q_matrix * this->verts[i]).stringify() + "\n";
    }
    temp += "\nMatrix rotations:\n" + this->_t_matrix.stringify();
    temp += "\nQuaternion rotations:\n" + this->_q_matrix.stringify();
    return temp;
}

// translation functions
void Cube::translateX(float amount) {
    Matrix<float> temp;
    temp(0, 3) = amount;
    this->_t_matrix = temp * this->_t_matrix;
    this->_q_matrix = temp * this->_q_matrix;
}

void Cube::translateY(float amount) {
    Matrix<float> temp;
    temp(1, 3) = amount;
    this->_t_matrix = temp * this->_t_matrix;
    this->_q_matrix = temp * this->_q_matrix;
}

void Cube::translateZ(float amount) {
    Matrix<float> temp;
    temp(2, 3) = amount;
    this->_t_matrix = temp * this->_t_matrix;
    this->_q_matrix = temp * this->_q_matrix;
}

void Cube::translateXYZ(float dx, float dy, float dz) {
    Matrix<float> temp;
    temp(0, 3) = dx;
    temp(1, 3) = dy;
    temp(2, 3) = dz;
    this->_t_matrix = temp * this->_t_matrix;
    this->_q_matrix = temp * this->_q_matrix;
}

void Cube::translateXYZ(Vector3<float> v) {
    Matrix<float> temp;
    temp(0, 3) = v[0];
    temp(1, 3) = v[1];
    temp(2, 3) = v[2];
    this->_t_matrix = temp * this->_t_matrix;
    this->_q_matrix = temp * this->_q_matrix;
}

Matrix<float> quatRotation(Vector3<float> axis, float theta) {
    float theta2 = 0.5 * theta;
    return Quaternion(axis.x() * sin(theta2), axis.y() * sin(theta2), axis.z() * sin(theta2), cos(theta2)).matrix();
}

// rotation functions
//rotate about the y axis for heading
void Cube::rotateH(float amount) {
    // matrix rotation
    Matrix<float> temp, temp2;
    float angle = RAD(amount);
    temp(0, 0) = cos(angle);
    temp(0, 2) = sin(angle);
    temp(2, 0) = -sin(angle);
    temp(2, 2) = cos(angle);
    this->_t_matrix = temp * this->_t_matrix;

    // quaternion rotation
    temp2 = quatRotation(Vector3<float>(0.0f, 1.0f, 0.0f), RAD(amount));
    this->_q_matrix = temp2 * this->_q_matrix;
}

// rotate about the x axis for pitch
void Cube::rotateP(float amount) {
    Matrix<float> temp, temp2;
    float angle = RAD(amount);
    temp(1, 1) = cos(angle);
    temp(2, 1) = sin(angle);
    temp(1, 2) = -sin(angle);
    temp(2, 2) = cos(angle);
    this->_t_matrix = temp * this->_t_matrix;

    // quaternion rotation
    temp2 = quatRotation(Vector3<float>(1.0f, 0.0f, 0.0f), RAD(amount));
    this->_q_matrix = temp2 * this->_q_matrix;
}

// rotate about the z axis for roll
void Cube::rotateR(float amount) {
    Matrix<float> temp, temp2;
    float angle = RAD(amount);
    temp(0, 0) = cos(angle);
    temp(1, 0) = sin(angle);
    temp(0, 1) = -sin(angle);
    temp(1, 1) = cos(angle);
    this->_t_matrix = temp * this->_t_matrix;

    // quaternion rotation
    temp2 = quatRotation(Vector3<float>(0.0f, 0.0f, 1.0f), RAD(amount));
    this->_q_matrix = temp2 * this->_q_matrix;
}

// rotate all 3
void Cube::rotateHPR(float dh, float dp, float dr) {
    rotateH(dh);
    rotateP(dp);
    rotateR(dr);
}

// scale
void Cube::scale(float amount) {
    Matrix<float> temp;
    temp *= amount;
    this->_t_matrix = temp * this->_t_matrix;
    this->_q_matrix = temp * this->_q_matrix;
}

// combined transform
void Cube::transformXYZHPRS(float dx, float dy, float dz, float dh, float dp, float dr, float s) {
    translateXYZ(dx, dy, dz);
    rotateHPR(dh, dp, dr);
    scale(s);
}

void showHelp() {
    std::cout << "translateX        amount\n"
              << "translateY        amount\n"
              << "translateZ        amount\n"
              << "translateXYZ      dx dy dz\n"
              << "rotateH           amount // degrees\n"
              << "rotateP           amount // degrees\n"
              << "rotateR           amount // degrees\n"
              << "rotateHPR         dh dp dr // all in degrees\n"
              << "scale             amount\n"
              << "transformXYZHPRS  dx dy dz dh dp dr s\n"
              << "Vector3           1/2 x y z (argument of 1 or 2 indicates which vector to set, 2 available)\n"
              << "Quaternion        1/2 x y z w (again, 1 or 2 indicates which of 2 quaternions to set\n"
              << "SLERP             beta (uses quaternions 1 and 2 as defined with Quaternion command)\n"
              << "LERP              beta (uses Vectors 1 and 2 as defined with Vector3 command)\n"
              << "reset\n"
              << "quit"
              << std::endl;
}

int main() {
    Cube cube = Cube();
    Vector3<float> vec1 = Vector3<float>();
    Vector3<float> vec2 = Vector3<float>();
    Quaternion quat1 = Quaternion();
    Quaternion quat2 = Quaternion();
    while(1) {
        std::cout << "Enter a function, or type help.\n";
        std::string input;
        std::cin >> input;
        if (input == "translateX") {
            float amt;
            std::cin >> amt;
            cube.translateX(amt);
        }
        else if (input == "translateY") {
            float amt;
            std::cin >> amt;
            cube.translateY(amt);
        }
        else if (input == "translateZ") {
            float amt;
            std::cin >> amt;
            cube.translateZ(amt);
        }
        else if (input == "translateXYZ" || input == "translate") {
            float dx, dy, dz;
            std::cin >> dx >> dy >> dz;
            cube.translateXYZ(dx, dy, dz);
        }
        else if (input == "rotateH") {
            float amt;
            std::cin >> amt;
            cube.rotateH(amt);
        }
        else if (input == "rotateP") {
            float amt;
            std::cin >> amt;
            cube.rotateP(amt);
        }
        else if (input == "rotateR") {
            float amt;
            std::cin >> amt;
            cube.rotateR(amt);
        }
        else if (input == "rotateHPR" || input == "rotate") {
            float dh, dp, dr;
            std::cin >> dh >> dp >> dr;
            cube.rotateHPR(dh, dp, dr);
        }
        else if (input == "scale") {
            float amt;
            std::cin >> amt;
            cube.scale(amt);
        }
        else if (input == "transformXYZHPRS" || input == "transform") {
            float dx, dy, dz, dh, dp, dr, s;
            std::cin >> dx >> dy >> dz >> dh >> dp >> dr >> s;
            cube.transformXYZHPRS(dx, dy, dz, dh, dp, dr, s);
        }
        else if (input == "Vector3") {
            unsigned int flag;
            float x, y, z;
            std::cin >> flag >> x >> y >> z;
            if (flag <= 1) {
                vec1[0] = x;
                vec1[1] = y;
                vec1[2] = z;
            }
            else {
                vec2[0] = x;
                vec2[1] = y;
                vec2[2] = z;
            }
        }
        else if (input == "Quaternion") {
            unsigned int flag;
            float x, y, z, w;
            std::cin >> flag >> x >> y >> z >> w;
            if (flag <= 1) {
                quat1.x() = x;
                quat1.y() = y;
                quat1.z() = z;
                quat1.w() = w;
                quat1.normalize();
            }
            else {
                quat2.x() = x;
                quat2.y() = y;
                quat2.z() = z;
                quat2.w() = w;
                quat2.normalize();
            }
        }
        else if (input == "SLERP") {
            float beta;
            std::cin >> beta;
            Quaternion n = quat1.SLERP(quat2, beta);
            std::cout << "Quaternion 1: " << quat1.q1() << "i + " << quat1.q2() << "j + " << quat1.q3() << "k + " << quat1.q0() << "\n"
                      << "Quaternion 2: " << quat2.q1() << "i + " << quat2.q2() << "j + " << quat2.q3() << "k + " << quat2.q0() << "\n"
                      << "Interpolated: " << n.q1() << "i + " << n.q2() << "j + " << n.q3() << " + " << n.q0() << "\n"
                      << n.matrix().stringify() << std::endl;
        }
        else if (input == "LERP") {
            float beta;
            std::cin >> beta;
            Vector3<float> n = vec1.LERP(vec2, beta);
            std::cout << "Vector 1: (" << vec1.x() << ", " << vec1.y() << ", " << vec1.z() << ")\n"
                      << "Vector 2: (" << vec2.x() << ", " << vec2.y() << ", " << vec2.z() << ")\n"
                      << "Interpolated: (" << n.x() << ", " << n.y() << ", " << n.z() << ")" << std::endl;
        }
        else if (input == "reset") {
            cube.reset();
        }
        else if (input == "quit" || input == "exit") {
            break;
        }
        else {
            std::cout << "Your input was: " << input << " which isn't recognized.\n"
                      << "Valid functions are: \n\n";
            showHelp();
            continue;
        }
        std::cout << cube.transform() << std::endl;
    }
    return 0;
}
