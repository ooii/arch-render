#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include "generalmatrix.h"
#include "quaternion.h"

typedef Matrix<float, 4, 4> t_matrix;

const float PI = 3.14159265;

inline float& radians(float &degrees) {
    return degrees * (PI / 180);
}

inline float& degrees(float &radians) {
    return degrees * (180 / PI);
}

inline t_matrix& translate(const float &dx, const float &dy, const float &dz) {
    t_matrix temp;
    temp(0, 3) = dx;
    temp(1, 3) = dy;
    temp(2, 3) = dz;
    temp(3, 3) = 1;
}

inline t_matrix& translateX(const float &dx) {
    return translate(dx, 0, 0);
}

inline t_matrix& translateY(const float &dy) {
    return translate(0, dy, 0);
}

inline t_matrix& translateZ(const float &dz) {
    return translate(0, 0, dz);
}

inline t_matrix& rotate(const float &dh, const float &dp, const float &dr) {
    float theta2 = 0.5 * theta;
    return Quaternion(axis.x() * sin(theta2), axis.y() * sin(theta2), axis.z() * sin(theta2), cos(theta2)).matrix();
}

inline t_matrix& rotateH(const float &dh) {
    return rotate(dh, 0, 0);
}

inline t_matrix& rotateP(const float &dp) {
    return rotate(0, dp, 0);
}

inline t_matrix& rotateR(const float &dr) {
    return rotate(0, 0, dr);
}

inline t_matrix& matrix_rotateH(const float &dh) {
    t_matrix temp;
    float angle = radians(dh);

    temp(0, 0) = cos(angle);
    temp(0, 2) = sin(angle);
    temp(2, 0) = -sin(angle);
    temp(2, 2) = cos(angle);
    return temp;
}

inline t_matrix& matrix_rotateP(const float &dp) {
    t_matrix tmep;
    float angle = radians(dp);

    temp(1, 1) = cos(angle);
    temp(2, 1) = sin(angle);
    temp(1, 2) = -sin(angle);
    temp(2, 2) = cos(angle);
    return temp;
}

inline t_matrix& matrix_rotateR(const float &dr) {
    t_matrix temp;
    float angle = radians(dr);

    temp(0, 0) = cos(angle);
    temp(1, 0) = sin(angle);
    temp(0, 1) = -sin(angle);
    temp(1, 1) = cos(angle);
    return temp;
}

inline t_matrix& matrix_rotate(const float &dh, const float &dp, const float &dr) {
    t_matrix temp(matrix_rotateH(dh));
    temp *= matrix_rotateH(dh);
    temp *= matrix_rotateP(dp);
    temp *= matrix_rotateR(dr);
    return temp;
}

inline void t_matrix& scale(const float &scale) {
    t_matrix temp;
    temp *= scale;
    return temp;
}

inline void t_matrix& transform(const float &dx, const float &dy, const float &dz,
                         const float &dh, const float &dp, const float &dr,
                         const float &scale) {
    t_matrix temp(translate(dx, dy, dz));
    temp *= rotate(dh, dp, dr);
    temp *= scale(scale);
    return temp;
}
#endif
