#include <math.h>
#include "matrix.h"

class Quaternion {
    public:
        Quaternion() {
            for (int i = 0; i < 3; i++) {
                this->_data[i] = 0.0f;
            }
            this->_q0 = 0.0f;
        }
        Quaternion(const float &x, const float &y, const float &z, const float &w) {
            this->_data[0] = x;
            this->_data[1] = y;
            this->_data[2] = z;
            this->_q0 = w;
        }
        Quaternion(Matrix<float> &mat) {
            // the silly next array
            unsigned int next[3] = {1, 2, 0};

            // s value
            float s;

            // initialize the quaternion to 0
            this->_data[0] = 0;
            this->_data[1] = 0;
            this->_data[2] = 0;
            this->_q0 = 0;
           
            // matrix trace 
            float trace = mat(0,0) + mat(1, 1) + mat(2, 2);

            if (trace > 0) {
                s = sqrt(trace + 1);
                this->_q0 = s * 0.5;
                s = 0.5 / s;
                this->_data[0] = (mat(2, 1) - mat(1, 2)) * s;
                this->_data[1] = (mat(0, 2) - mat(2, 0)) * s;
                this->_data[2] = (mat(1, 0) - mat(0, 1)) * s;
            }
            else {
                // find the max of 0,0 1,1 and 2,2
                int i = 0, j, k;
                if (mat(1, 1) > mat(0, 0)) {
                    i = 1;
                }
                if (mat(2, 2) > mat(i, i)) {
                    i = 2;
                }
                // choose i, j, and k indices
                j = next[i];
                k = next[k];

                // set up s value
                s = sqrt((mat(i, i) - (mat(j, j) + mat(k, k))) + 1);
                this->_data[i] = s * 0.5;
                if (s != 0) {
                    s = 0.5 / s;
                }

                // setup the quaternion values
                this->_q0 = (mat(j, k) - mat(k, j) * s);
                this->_data[j] = (mat(i, j) + mat(j, i)) * s;
                this->_data[k] = (mat(i, k) + mat(k, i)) * s;
            }
            this->normalize();
        }
        inline float& q1() {
            return this->_data[0];
        }
        inline const float& q1() const {
            return this->_data[0];
        }
        inline float& q2() {
            return this->_data[1];
        }
        inline const float& q2() const {
            return this->_data[1];
        }
        inline float& q3() {
            return this->_data[2];
        }
        inline const float& q3() const {
            return this->_data[2];
        }
        inline float& q0() {
            return this->_q0;
        }
        inline const float& q0() const {
            return this->_q0;
        }
        inline float& x() {
            return this->_data[0];
        }
        inline const float& x() const {
            return this->_data[0];
        }
        inline float& y() {
            return this->_data[1];
        }
        inline const float& y() const {
            return this->_data[1];
        }
        inline float& z() {
            return this->_data[2];
        }
        inline const float& z() const {
            return this->_data[2];
        }
        inline float& w() {
            return this->_q0;
        }
        inline const float& w() const {
            return this->_q0;
        }
        inline const float magnitude() const {
            float tmp;
            for (int i = 0; i < 4; i++) {
                tmp += this->_data[i] * this->_data[i];
            }
            return sqrt(tmp);
        }
        inline Quaternion& normalize() {
            float m = this->magnitude();
            for (int i = 0; i < 4; i++) {
                this->_data[i] /= m;
            }
            return *this;
        }
        inline Matrix<float> matrix() {
            Matrix<float> temp = Matrix<float>(0.0f);

            temp[0] = 1 - (2 * q2() * q2()) - (2 * q3() * q3());
            temp[1] = (2 * q1() * q2()) - (2 * q0() * q3());
            temp[2] = (2 * q1() * q3()) + (2 * q0() * q2());
            temp[3] = 0;
            temp[4] = (2 * q1() * q2()) + (2 * q0() * q3());
            temp[5] = 1 - (2 * q1() * q1()) - (2 * q3() * q3());
            temp[6] = (2 * q2() * q3()) - (2 * q0() * q1());
            temp[7] = 0;
            temp[8] = (2 * q1() * q3()) - (2 * q0() * q2());
            temp[9] = (2 * q2() * q3()) + (2 * q0() * q1());
            temp[10] = 1 - (2 * q1() * q1()) - (2 * q2() * q2());
            temp[11] = temp[12] = temp[13] = temp[14] = 0;
            temp[15] = 1;

            return temp;
        }
        inline Quaternion operator*(const Quaternion &other) {
            return Quaternion(this->q1() * other.q0() + this->q0() * other.q1() + this->q2() * other.q3() - this->q3() * other.q2(),
                               this->q2() * other.q0() + this->q0() * other.q2() + this->q3() * other.q1() - this->q1() * other.q3(),
                               this->q3() * other.q0() + this->q0() * other.q3() + this->q1() * other.q2() - this->q2() * other.q1(),
                               this->q0() * other.q0() + this->q1() * other.q1() + this->q2() * other.q2() - this->q3() * other.q3());
        }
        inline Quaternion operator*(const float &val) const {
            return Quaternion(this->x() * val, this->y() * val, this->z() * val, this->w() * val);
        }
        inline Quaternion operator+(const Quaternion &other) const {
            return Quaternion(this->x() + other.x(), this->y() + other.y(), this->z() + other.z(), this->w() + other.w());
        }
        inline Quaternion operator-(const Quaternion &other) const {
            return Quaternion(this->x() - other.x(), this->y() - other.y(), this->z() - other.z(), this->w() - other.w());
        }
        inline float dot3(const Quaternion &other) {
            return this->x() * other.x() + this->y() * other.y() + this->z() * other.z();
        }
        inline Quaternion SLERP(const Quaternion &q, const float &B) {
            //float theta = acos((Vector3<float>(p._data)).dot((Vector3<float>(q._data))));
            float theta = acos(this->dot3(q));
            float sinth = sin(theta);
            return ((*this) * (sin((1-B) * theta) / sinth)) + (q * (sin(B * theta) / sinth));
        }
    private: 
        float _data[3];
        float _q0;
};
